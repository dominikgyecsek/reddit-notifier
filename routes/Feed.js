const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Post = require('../models/Post.js');
const Channel = require('../models/Channel.js');

/**
* Gets a part of the user's feed (timeline of all channels, saved posts or specific channel)
* @param {string} id id of the current user (payload)
* @param {string} type type of the channel - timeline, saved or channel (param)
* @param {string} id of the channel (param)
* @param {string} keywords keywords to filter by (query)
* @param {string} page pagination page number (query)
* @return {[object]} array of matching posts
*/
router.get('/:type/:id', auth.required, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let type = req.params.type;
        let id = req.params.id;
        let page = parseInt(req.query.page) || 0;
        let keywords = req.query.keywords || ""

        console.log("GETTING FEED", id, type, page)

        let posts;

        let searchQuery = {
            userId: req.payload.id,
            isDeleted: { $ne: true }
        }

        if (keywords && keywords.trim()) {
            searchQuery.searchStr = {$regex : ".*" + keywords.trim().toLowerCase() + ".*"}
        }

        if (type === "timeline") {

        } else if (type === "saved") {
            searchQuery.isBookmarked = true
        } else if (type === "channel") {
            searchQuery.channelId = id
        }

        if (type === "timeline" || type === "saved") {

            posts = await Post
                .find(searchQuery)
                .sort({ updatedAt: -1 })
                .skip(page*25)
                .limit(25)
                .catch(e => { console.error("Failed to get feed", e) });

            if (!posts) {
                return reject({ code: 500, msg: "Failed to get feed" });
            }

            return resolve({
                posts: posts
            });

        } else if (type === "channel") {

            posts = await Post
                .find(searchQuery)
                .sort({ updatedAt: -1 })
                .skip(page*25)
                .limit(25)
                .catch(e => { console.error("Failed to get feed", e) });

            if (!posts) {
                return reject({ code: 500, msg: "Failed to get feed" });
            }

            let details = null;

            console.log("HERE WE GO "  + page)

            if (page === 0 || page === "0") {
                console.log("GETTING DETAIL" + req.payload.id + " " + id);
                details = await Channel.findOne({ userId: req.payload.id, _id: id }).catch(e => { console.error("Failed to get feed", e) });
            }

            return resolve({
                posts: posts,
                details: details
            });

        }

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

module.exports = router;
