import React, { Component } from 'react';
import Highlighter from "react-highlight-words";
import "./Post.css";

import Utilities from '../Utilities';
import Server from "../Server";

import FolderIcon from '-!svg-react-loader!../icons/folder.svg';
import LinkIcon from '-!svg-react-loader!../icons/link.svg';
import CalendarIcon from '-!svg-react-loader!../icons/calendar.svg';
import PersonIcon from '-!svg-react-loader!../icons/person.svg';
import OpenIcon from '-!svg-react-loader!../icons/open.svg';
import TrashIcon from '-!svg-react-loader!../icons/trash.svg';
import BookmarkIcon from '-!svg-react-loader!../icons/bookmark.svg';
import BookmarkOutlineIcon from '-!svg-react-loader!../icons/bookmark-outline.svg';

const MAX_SHORT_CHARS = 540;

class Post extends Component {

    constructor(props) {
        super(props);
        this.state = props.post
        this.state.showMore = false;
    }

    toggleBookmark() {

        console.log("TOGGLING BOOKmar", this.state.isBookmarked)
        this.setState({ isBookmarked: !this.state.isBookmarked })

        Server.api({
            method: "PUT",
            url: "/api/post/" + this.state._id + "/bookmark",
            then: function(res) {
                console.log("Bookmark toggle");
            }.bind(this),
            catch: function(errorMsg) {
                console.log("FAILED TO bookmark", errorMsg)
            }.bind(this)
        })

    }

    remove() {

        if (window.confirm("Are you sure you want to delete this post?")) {

            Server.api({
                method: "DELETE",
                url: "/api/post/" + this.state._id + "/",
                then: function(res) {
                    console.log("DEELTED");
                    this.props.onDelete();
                }.bind(this),
                catch: function(errorMsg) {
                    console.log("FAILED TO DEELTE", errorMsg)
                }.bind(this)
            })

        }

    }

    render() {

        return (
            <div className="post">
                <h3>
                    { ( ( this.props.lastReviewedDate && this.state.updatedAt && ( (this.props.lastReviewedDate) < (Math.round(new Date(this.state.updatedAt).getTime()/1000)) ) ) ) &&
                        <span>NEW</span>
                    }
                {this.state.meta.title}</h3>
                { (this.state.meta.body) &&
                    <div className={this.state.meta.thumbnailMedium ? "body-thumbnail" : ""}>
                        { (this.state.meta.thumbnailMedium) &&
                            <div className="thumbnail">
                                <img src={this.state.meta.thumbnailMedium} />
                            </div>
                        }
                        <div className="body" onClick={() => { this.setState({ showMore: !this.state.showMore }) }}>
                            <Highlighter
                                highlightClassName="match"
                                searchWords={this.state.matchedWords}
                                autoEscape={true}
                                textToHighlight={ this.state.showMore ? this.state.meta.body : Utilities.shortenToChar(this.state.meta.body, MAX_SHORT_CHARS) }
                            />
                            { (!this.state.showMore && this.state.meta.body.length > MAX_SHORT_CHARS) &&
                                <label className="show-more-indicator">Show More...</label>
                            }
                        </div>
                    </div>
                }
                { (!this.state.meta.body && this.state.meta.thumbnailLarge && !this.state.meta.video) &&
                    <div className="big-preview">
                        <img src={this.state.meta.thumbnailLarge} />
                    </div>
                }
                { (this.state.meta.video) &&
                    <div className="video">
                        <video controls>
                            <source src={this.state.meta.video} type="video/mp4" />
                        </video>
                    </div>
                }
                { (this.state.links && this.state.links.length !== 0) &&
                    <div className="links">
                        { this.state.links.map(link => {
                            return (
                                <a href={link.url} target="_blank">
                                    <LinkIcon />
                                    <span>{link.name}</span> <span>{link.url}</span>
                                </a>
                            )
                        }) }
                    </div>
                }
                <div className="options">
                    { (window.globalVars && window.globalVars.channelNames && window.globalVars.channelNames[this.state.channelId]) &&
                        <button>
                            <FolderIcon />
                            <label>{window.globalVars.channelNames[this.state.channelId]}</label>
                        </button>
                    }
                    { this.state.meta.created &&
                        <button>
                            <CalendarIcon />
                            <label>{Utilities.formatDate(this.state.meta.created, "HH:MM DD/MM/YYYY")}</label>
                        </button>
                    }
                    { this.state.meta.author &&
                        <button onClick={() => { window.open("https://reddit.com/u/" + this.state.meta.author) }}>
                            <PersonIcon />
                            <label>{this.state.meta.author}</label>
                        </button>
                    }
                    <button onClick={ () => { this.toggleBookmark() } }>
                        { (this.state.isBookmarked) ?
                            <BookmarkIcon />
                            :
                            <BookmarkOutlineIcon />
                        }
                        <label></label>
                    </button>
                    <button onClick={() => { this.remove(); }}>
                        <TrashIcon />
                        <label></label>
                    </button>
                    { this.state.meta.permalink &&
                        <button onClick={() => { window.open("https://reddit.com" + this.state.meta.permalink) }}>
                            <OpenIcon />
                            <label>Open on Reddit</label>
                        </button>
                    }
                </div>
            </div>
        )

    }

}

export default Post;
