const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Post = require('../models/Post.js');

/**
* Removes a post of the current user
* @param {string} id id of the current user (payload)
* @param {string} id id of the post (param)
* @return {string} id of the modified post
*/
router.delete('/:id', auth.required, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let deletedPost = await Post.findOneAndDelete({ _id: req.params.id, userId: req.payload.id }).catch(e => { console.error("Failed to delete post", e) });

        if (!deletedPost) {
            return reject({ code: 500, msg: "Failed to delete post" });
        }

        return resolve(req.params.id);

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

/**
* Toogles the bookmarked flag on a post from the current user
* @param {string} id id of the current user (payload)
* @param {string} id id of the post (param)
* @return {string} id of the modified post
*/
router.put('/:id/bookmark', auth.required, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let post = await Post.findOne({ _id: req.params.id, userId: req.payload.id }).catch(e => { console.error("Failed to get post", e) });

        if (!post) {
            return reject({ code: 404, msg: "Not found" });
        }

        post.isBookmarked = !post.isBookmarked;
        await post.save().catch(e => { console.error("Failed to toggle bookmark") });

        return resolve(req.params.id);

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

module.exports = router;
