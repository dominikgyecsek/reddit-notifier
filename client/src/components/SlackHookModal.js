import React, { Component } from 'react';
import { Modal, ModalHeader } from 'reactstrap';
import { WithContext as ReactTags } from 'react-tag-input';
import QueryBuilder, { formatQuery } from 'react-querybuilder';
import Server from '../Server';
import './SlackHookModal.css';

class SlackHookModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            id: null,
            name: "",
            buttonEnabled: true,
            slackUrl: ""
        };
    }

    componentWillReceiveProps(nextProps) {

        this.setState({ modal: nextProps.id ? true : false });

        if (nextProps.url) {
            this.setState({ slackUrl: nextProps.url })
        }

    }

    toggle = () => {
        this.props.toggle();
    }

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.save(e);
        }
    }

    disableButton = () => { this.setState({ buttonEnabled: false }) }
    enableButton = () => { this.setState({ buttonEnabled: true }) }

    onInputChange = (e) => {
        let modifiedField = e.target.id;
        let modifiedFieldValue = e.target.value;
        let state = {...this.state};
        state[modifiedField] = modifiedFieldValue;
        this.setState(state);
    }

    save = (e) => {

        if (e.target.getAttribute("data-enabled") !== "true") {
            return;
        }

        this.setState({ errorMsg: null })
        this.disableButton();

        Server.api({
            method: "PUT",
            url: "/api/channel/" + this.props.id + "/slackhookurl",
            data: {
                slackUrl: this.state.slackUrl
            },
            then: function(res) {
                console.log("SAVED!", res);
                this.toggle();
            }.bind(this),
            catch: function(errorMsg) {
                console.log(errorMsg);
                this.setState({ errorMsg: errorMsg })
                this.enableButton();
            }.bind(this)
        })

    }

    render() {

        return (
            <Modal isOpen={this.state.modal} toggle={this.toggle} className="slack-hook-modal">
                <ModalHeader toggle={this.toggle}>Slack Notifications</ModalHeader>
                <div className="body">
                    <div className="form-group">
                        <label for="email">Slack Hook URL</label>
                        <input data-enabled={this.state.buttonEnabled} onKeyPress={this.handleKeyPress} onChange={this.onInputChange} value={this.state.slackUrl} type="text" name="slackUrl" id="slackUrl" placeholder="Slack Hook URL" />
                    </div>
                    <div className="form-group">
                        <label>How to get a URL</label>
                        <p>1. Log into Slack</p>
                        <p>2. Go <a href="https://api.slack.com/apps?new_app=1" target="_blank"> to apps page on Slack</a></p>
                        <p>3. Click on create new app, name the app (can be anything) and select the workplace and click Save</p>
                        <p>4. Once redirected click on "Incoming Webhooks" and toggle it on</p>
                        <p>5. Click on "Add New Webhook to Workplace", select a channel you want the notificatios to be sent  and click "Allow"</p>
                        <p>6. Click on "Copy" on the newly created url and paste it here</p>
                        <p>To disable notifications remove the link and click save</p>
                    </div>
                    { (this.state.errorMsg) &&
                        <div className="error-msg" style={{ marginBottom: 20 }}>{this.state.errorMsg}</div>
                    }
                    <button data-enabled={this.state.buttonEnabled} className="button primary" onClick={(e) => { this.save(e); }}>Save</button>
                </div>
            </Modal>
        );

    }

}

export default SlackHookModal;
