import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './About.css';

class About extends Component {

    constructor(props) {
        super(props);
        this.FRONTEND_LIBS = [
            {
                name: "svg-react-loader",
                licence: "MIT",
                link: "https://github.com/jhamlet/svg-react-loader/blob/master/LICENSE"
            },
            {
                name: "babel-polyfill",
                licence: "MIT",
                link: "https://github.com/babel/babel/blob/master/LICENSE"
            },
            {
                name: "axios",
                licence: "MIT",
                link: "https://github.com/axios/axios/blob/master/LICENSE"
            },
            {
                name: "bootstrap",
                licence: "MIT",
                link: "https://github.com/twbs/bootstrap/blob/master/LICENSE"
            },
            {
                name: "react-highlight-words",
                licence: "MIT",
                link: "https://github.com/bvaughn/react-highlight-words/blob/master/LICENSE"
            },
            {
                name: "react-masonry-css",
                licence: "MIT",
                link: "https://github.com/paulcollett/react-masonry-css/blob/master/LICENSE"
            },
            {
                name: "react-querybuilder",
                licence: "MIT",
                link: "https://github.com/sapientglobalmarkets/react-querybuilder/blob/master/LICENSE.md"
            },
            {
                name: "react-tag-input",
                licence: "MIT",
                link: "https://github.com/prakhar1989/react-tags/blob/master/LICENSE"
            },
            {
                name: "reactstrap",
                licence: "MIT",
                link: "https://github.com/reactstrap/reactstrap/blob/master/LICENSE"
            },
            {
                name: "ionic (Icons)",
                licence: "MIT",
                link: "https://github.com/ionic-team/ionic/blob/master/LICENSE"
            },
        ]

        this.BACKEND_LIBS = [
            {
                name: "body-parser",
                licence: "MIT",
                link: "https://github.com/expressjs/body-parser/blob/master/LICENSE"
            },
            {
                name: "compression",
                licence: "MIT",
                link: "https://github.com/expressjs/compression/blob/master/LICENSE"
            },
            {
                name: "express",
                licence: "MIT",
                link: "https://github.com/expressjs/express/blob/master/LICENSE"
            },
            {
                name: "express-jwt",
                licence: "MIT",
                link: "https://github.com/auth0/express-jwt/blob/master/LICENSE"
            },
            {
                name: "express-rate-limit",
                licence: "MIT",
                link: "https://github.com/nfriedly/express-rate-limit/blob/master/LICENSE"
            },
            {
                name: "jsonwebtoken",
                licence: "MIT",
                link: "https://github.com/auth0/node-jsonwebtoken/blob/master/LICENSE"
            },
            {
                name: "mongoose",
                licence: "MIT",
                link: "https://github.com/Automattic/mongoose/blob/master/LICENSE.md"
            },
            {
                name: "node-cron",
                licence: "ISC",
                link: "https://github.com/node-cron/node-cron/blob/master/LICENSE.md"
            },
            {
                name: "node-rss",
                licence: "MIT",
                link: "https://github.com/dylang/node-rss/blob/master/LICENSE"
            },
            {
                name: "snoowrap",
                licence: "MIT",
                link: "https://github.com/not-an-aardvark/snoowrap/blob/master/LICENSE.md"
            },
            {
                name: "validator",
                licence: "MIT",
                link: "https://github.com/validatorjs/validator.js/blob/master/LICENSE"
            },
            {
                name: "QueryParser from Honours Project",
                link: "https://github.com/d0minYk/the-open-data-map/blob/master/models/QueryParser.js"
            }
        ]

        this.APIs = [
            {
                name: "Reddit",
                link: "https://www.reddit.com/dev/api/"
            },
        ]

    }

    componentDidMount() {
        document.title = window.globalVars.pageTitleBefore + "About" + window.globalVars.pageTitleAfter;
        window.scrollTo({top: 0, left: 0, behavior: "smooth"});
    }

    render() {

        return (
            <div className="about-page">

                <div className="main-container">

                    <div className="intro">
                        <h1>Reddit Notifier</h1>
                        <p>Reddit is an awesome place to get your latest memes, keep updated of latest news, get help, look for a job and for virtually anything else.</p>
                        <p>But there are just so many stuff that you can spend hours just scrolling aimlessly and Reddit’s search is like no search at all. Reddit Notifier allows you to set up advanced filters so you only see what you really want to see on your personalized feed.</p>
                        <p>You can also set up notifications sent to Slack or accessible via an RSS feed so you can keep in the loop on the go.</p>
                    </div>

                    <h2>Credits</h2>

                    <h3>Front-end</h3>
                    <ul>
                        { this.FRONTEND_LIBS.map(item => {
                            return (
                                <li>
                                    <a href={item.link} target="_blank">
                                        {item.name} - {item.licence}
                                    </a>
                                </li>
                            )
                        }) }
                    </ul>

                    <h3>Back-end</h3>
                    <ul>
                        { this.BACKEND_LIBS.map(item => {
                            return (
                                <li>
                                    <a href={item.link} target="_blank">
                                        {item.name} - {item.licence}
                                    </a>
                                </li>
                            )
                        }) }
                    </ul>

                    <h3>APIs</h3>
                    <ul>
                        { this.APIs.map(item => {
                            return (
                                <li>
                                    <a href={item.link} target="_blank">
                                        {item.name}
                                    </a>
                                </li>
                            )
                        }) }
                    </ul>

                </div>

            </div>
        );

    }

}

export default About;
