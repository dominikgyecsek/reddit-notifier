const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TemplateSchema = new Schema({
    name: String, // Name of the template
    subreddits: [String], // Array of subreddit names to scan
    query: Schema.Types.Mixed, // Query object to match posts against
    querySQL: String, // SQL-like formatted query string for easier understanding
}, {
	collection: 'templates',
	timestamps: true
});

module.exports = mongoose.model('Temaplte', TemplateSchema);
