import CounterReducer from './counter';

import { combineReducers } from "redux"

const allReducers = combineReducers({
    counter: CounterReducer
}) 

export default allReducers;