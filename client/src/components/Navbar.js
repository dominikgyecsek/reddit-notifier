import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

import NavBarStyles from './Navbar.css';
import Logo from '../images/logo.png';
import Server from '../Server';

import LogOutIcon from '-!svg-react-loader!../icons/log-out.svg';

export default class NavbarMain extends React.Component {

  constructor(props) {

    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      dropdownOpen: false,
      user: {

      }
    };

  }

  toggle() {
    this.setState({
        isOpen: !this.state.isOpen
    });
  }

  hide() {
    this.setState({
        isOpen: false,
    });
  }

  toggleDropdown() {
    this.setState({
        dropdownOpen: !this.state.dropdownOpen,
    });
  }

  logout = () => {
      Server.logout();
  }

  componentDidMount() {
      this.setState({ loggedIn: ((localStorage.getItem("authToken")) ? true : false) })
  }

  render() {

    return (

        <div className="navbar-container no-print" data-open={this.state.isOpen}>
             <div className="main-container">
        <Navbar light expand="md">
          <Link onClick={() => this.hide()} className="navbar-brand" to={ this.state.loggedIn ? "/dashboard/timeline" : "/home" }>
              <img className="logo" src={Logo} />
          </Link>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
                { (!this.state.loggedIn) &&
                    <NavItem><Link onClick={() => this.hide()} className="nav-link" to="/home">Home</Link></NavItem>
                }
                <NavItem><Link onClick={() => this.hide()} className="nav-link" to="/templates">Templates</Link></NavItem>
                <NavItem><Link onClick={() => this.hide()} className="nav-link" to="/about">About</Link></NavItem>
                <NavItem><Link onClick={() => this.hide()} className="nav-link" to="/terms">Terms</Link></NavItem>
                { (this.state.loggedIn) &&
                    <NavItem style={{ cursor: 'pointer' }}><a onClick={() => this.logout()} className="nav-link">Log out</a></NavItem>
                }

            </Nav>
          </Collapse>
        </Navbar>
        </div>
      </div>
    );

  }

}
