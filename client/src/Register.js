import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Server from './Server';
import './Register.css';

class Register extends Component {

    constructor(props) {

        super(props);

        this.state = {
            email: "",
            password: "",
            errorMsg: "",
            buttonEnabled: true
        };

    }

    disableButton = () => { this.setState({ buttonEnabled: false }) }
    enableButton = () => { this.setState({ buttonEnabled: true }) }

    onInputChange = (e) => {
        let modifiedField = e.target.id;
        let modifiedFieldValue = e.target.value;
        let state = {...this.state};
        state[modifiedField] = modifiedFieldValue;
        this.setState(state);
    }

    signup = (e) => {

        if (e.target.getAttribute("data-enabled") !== "true") {
            return;
        }

        this.disableButton();

        Server.api({
            method: "post",
            url: "/api/user",
            data: {
                email: this.state.email,
                password: this.state.password,
            },
            then: function(res) {
                // Automatically logging in
                if (res.data.token) {
                    localStorage.setItem("authToken", res.data.token)
                    localStorage.setItem("user", JSON.stringify(res.data))
                    this.props.history.push("/dashboard/timeline")
                }
            }.bind(this),
            catch: function(errorMsg) {
                this.setState({ errorMsg: errorMsg })
                this.enableButton();
            }.bind(this)
        })

    }

    componentDidMount() {
        document.title = window.globalVars.pageTitleBefore + "Register" + window.globalVars.pageTitleAfter;
        window.scrollTo({top: 0, left: 0, behavior: "smooth"});
    }

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.signup(e);
        }
    }

    render() {

        return (
            <div className="register-page">
                <div className="main-container">
                    <h1>Register</h1>
                    <div className="form">
                        <div className="form-group">
                            <label for="email">Email address</label>
                            <input data-enabled={this.state.buttonEnabled} onKeyPress={this.handleKeyPress} onChange={this.onInputChange} defaultValue={this.state.email} type="email" name="email" id="email" placeholder="Email address" />
                        </div>
                        <div className="form-group">
                            <label for="password">Password</label>
                            <input
                                data-enabled={this.state.buttonEnabled}
                                onKeyPress={this.handleKeyPress}
                                onChange={this.onInputChange}
                                defaultValue={this.state.password}
                                type="password"
                                name="password"
                                id="password"
                                placeholder="Password"
                            />
                        </div>
                        <div className="terms">
                            By signing up you agree to our <a target="_blank" href="/terms">Terms & Conditions</a>
                        </div>
                        <button data-enabled={this.state.buttonEnabled} className="register-btn button primary" onClick={(e) => {this.signup(e)}}>
                            Sign Up
                        </button>
                        <Link to="/login" style={{ color: '#333' }}>Already have an account?</Link>
                        { (this.state.errorMsg) &&
                            <div className="error-msg">{this.state.errorMsg}</div>
                        }
                    </div>
                </div>
            </div>
        );

    }

}

export default Register;
