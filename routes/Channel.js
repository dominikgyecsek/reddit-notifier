const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const keys = require('../config/keys.js');
const Channel = require('../models/Channel.js');
const QueryParser = require('../models/QueryParser.js');
const snoowrap = require('snoowrap');
const r = new snoowrap(keys.redditAuth);

/**
* @param {string} id id of the current user (payload)
* @return {[object]} list of channels of the current user
*/
router.get('/', auth.required, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let channels = Channel.find({ userId: req.payload.id }, "name subreddits").catch(e => { console.error("Failed to get channels", e) });

        if (!channels) {
            return reject({ code: 404, msg: "No channels found" });
        }

        return resolve(channels);

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

/**
* Deletes a channel of the current user
* @param {string} id id of the current user (payload)
* @param {string} id id of the channel (param)
* @return {object} deleted channel
*/
router.delete('/:id', auth.required, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let deletedChannel = Channel.findOneAndDelete({ _id: req.params.id, userId: req.payload.id }).catch(e => { console.error("Failed to delete channel", e) });

        if (!deletedChannel) {
            return reject({ code: 404, msg: "Not found" });
        }

        return resolve(deletedChannel);

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

/**
* @param {string} id id of the current user (payload)
* @param {string} id id of the channel (param)
* @return {object} a channel of the current user
*/
router.get('/:id', auth.required, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let channel = Channel.findOne({ _id: req.params.id, userId: req.payload.id }).catch(e => { console.error("Failed to get channel", e) });

        if (!channel) {
            return reject({ code: 404, msg: "Not found" });
        }

        return resolve(channel);

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

/**
* @param {string} id id of the current user (payload)
* @param {string} id id of the channel (param)
* @param {string} slackUrl slack hook urlr (body)
* @return {string} the updated channel's id
*/
router.put('/:id/slackhookurl', auth.required, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let channelId = req.params.id;
        let slackUrl = req.body.slackUrl;

        console.log("PUTTING " + slackUrl);

        if (!slackUrl) {
            // Remove notifications
            slackUrl = "";
        } else if (!slackUrl.startsWith("https://hooks.slack.com/services/")) {
            return reject({ code: 400, msg: "Invalid url" });
        }

        let updatedChannel = await Channel.findOneAndUpdate({ _id: channelId, userId: req.payload.id }, {
            slackUrl: slackUrl,
        }).catch(e => { console.error("Failed to update channel", e) });

        if (!updatedChannel) {
            return reject({ code: 500, msg: "Failed to update channel" });
        }

        return resolve(req.params.id);

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

/**
* Updates an existing channel of the current user
* @param {string} id id of the current user (payload)
* @param {string} id id of the channel (param)
* @param {string} name name of the channel (body)
* @param {[string]} subreddits array of subreddit names to scan (body)
* @param {string} query query object to match posts against (body)
* @param {string} querySQL SQL-like formatted query string for easier understanding (body)
* @return {string} the updated channel's id
*/
router.put('/:id', auth.required, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let channel = req.body;

        if(!channel.name || !channel.name.toLowerCase()) {
            return reject({ code: 400, msg: "Name is required" });
        }

        if(!channel.subreddits || !Array.isArray(channel.subreddits) || channel.subreddits.length === 0) {
            return reject({ code: 400, msg: "At least one subreddit is required" });
        }

        // Validate subreddit names
        for (let i = 0; i < channel.subreddits.length; i++) {
            let subredditName = channel.subreddits[i].text;
            let latestPosts = await r.getSubreddit(subredditName).getNew({limit: 1}).catch(e => { console.error("Failed to get subreddti", e) });
            if (!latestPosts || latestPosts.length === 0) {
                return reject({ code: 400, msg: "\"" + subredditName + "\" does not appear to be a valid subreddit" });
            }
        }

        if (!channel.query) {
            return reject({ code: 400, msg: "No query object defined" });
        }

        if ( (channel.query && channel.query.rules && Array.isArray(channel.query.rules) && channel.query.rules.length === 0) ) {
            // Empty query -> all posts are going to be matched
        } else {
            // Checking for empty values
            let rules = QueryParser.recursiveExpand(channel.query.combinator, channel.query.rules);
            for (let i = 0; i < rules.length; i++) {
                if (rules[i].value.trim() === "") {
                    return reject({ code: 400, msg: "Empty values detected" });
                }
            }
        }

        let updatedChannel = await Channel.findOneAndUpdate({ _id: req.params.id, userId: req.payload.id }, {
            name: channel.name,
            subreddits: channel.subreddits.map(item => item.text),
            query: channel.query,
            querySQL: channel.querySQL
        }).catch(e => { console.error("Failed to update channel", e) });

        if (!updatedChannel) {
            return reject({ code: 500, msg: "Failed to update channel" });
        }

        return resolve(req.params.id);

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

/**
* Creates a new channel
* @param {string} id id of the current user (payload)
* @param {string} name name of the channel (body)
* @param {[string]} subreddits array of subreddit names to scan (body)
* @param {string} query query object to match posts against (body)
* @param {string} querySQL SQL-like formatted query string for easier understanding (body)
* @return {string} the new channel's id
*/
router.post('/', auth.required, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let channel = req.body;

        if(!channel.name || !channel.name.toLowerCase()) {
            return reject({ code: 400, msg: "Name is required" });
        }

        if(!channel.subreddits || !Array.isArray(channel.subreddits) || channel.subreddits.length === 0) {
            return reject({ code: 400, msg: "At least one subreddit is required" });
        }

        // Validate subreddit names
        for (let i = 0; i < channel.subreddits.length; i++) {
            let subredditName = channel.subreddits[i].text;
            let latestPosts = await r.getSubreddit(subredditName).getNew({limit: 1}).catch(e => { console.error("Failed to get subreddti", e) });
            if (!latestPosts || latestPosts.length === 0) {
                return reject({ code: 400, msg: "\"" + subredditName + "\" does not appear to be a valid subreddit" });
            }
        }

        if (!channel.query) {
            return reject({ code: 400, msg: "No query object defined" });
        }

        if ( (channel.query && channel.query.rules && Array.isArray(channel.query.rules) && channel.query.rules.length === 0) ) {
            // Empty query -> all posts are going to be matched
        } else {
            // Checking for empty values
            let rules = QueryParser.recursiveExpand(channel.query.combinator, channel.query.rules);
            for (let i = 0; i < rules.length; i++) {
                if (rules[i].value.trim() === "") {
                    return reject({ code: 400, msg: "Empty values detected" });
                }
            }
        }

        let newChannel = new Channel({
            name: channel.name,
            subreddits: channel.subreddits.map(item => item.text),
            query: channel.query,
            querySQL: channel.querySQL,
            userId: req.payload.id
        });

        let savedChannel = await newChannel.save().catch(e => { console.error("Failed to save channel", e) });

        if (!savedChannel) {
            return reject({ code: 500, msg: "Failed to save channel" });
        }

        return resolve(savedChannel._id);

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

module.exports = router;
