const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
    userId: Schema.Types.ObjectId, // Id of the user who owns the post
    channelId: Schema.Types.ObjectId, // Id of the channel the post was matched against
    isBookmarked: Boolean, // Whether a post if bookmarked
    isDeleted: Boolean, // Whether a post is deleted
    matchedWords: [String], // Words or phrases in the post that matched the query object
    meta: Schema.Types.Mixed, // Details about the reddit post (body, author, title, created, permalink, video: video, videoThumbnail, thumbnail, thumbnailMedium: thumbnailMedium, thumbnailLarge, image)
    links: [Schema.Types.Mixed], // Array of liks extracted from the post's body
    searchStr: String, // Body and title of the reddit post for easy search
}, {
	collection: 'posts',
	timestamps: true
});

module.exports = mongoose.model('Post', PostSchema);
