const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('./auth');
const validator = require('validator');
var crypto = require('crypto');
const User = require('../models/User.js');
const NoConsentUsers = require('../models/NoConsentUsers.js');
const CV = require('../models/CV.js');
const Graduate = require('../models/Graduate.js');
const Company = require('../models/Company.js');
const Business = require('../models/Business.js');
const Utilities = require('../Utilities');
const Email = require('../models/Email');
const MessageThreadModel = require('../models/MessageThread');
const OneClickSignup = require('../models/OneClickSignup');
const Slack = require('../models/Slack');
const keys = require('../config/keys');
const bullhornAPI = require('../models/bullhornAPI');

router.post('/restore/email', (req, res, next) => {

    const { body: { user } } = req;
    const terms = req.body.terms;

    if(!user.email) {
        return res.json({
            success: false,
            message: "Please enter an email address."
        })
    }

    if (!validator.isEmail(user.email)) {
        return res.json({
            success: false,
            message: "Please enter a valid email address."
        })
    }

    User
        .find({'email': user.email}, "email firstName type")
        .exec(function(err, user) {

            if ( (err) || (user === undefined) || (user === null) || (user.length === 0) ) {

                return res.json({
                    success: false,
                    message: "We couldn't find your email"
                })

            } else {

                return crypto.randomBytes(48, function(err, buffer) {

                    user = user[0];
                    let userTwo = user.type;
                    user.passwordRestoreHash = buffer.toString('hex');
                    user.passwordRestoreExpiry = new Date(new Date().getTime() + 60000*1000);
                    user.save(function(err, user) {

                        if (err) {

                            return res.json({
                                success: false,
                                message: "Unexpected Error"
                            })

                        } else {

                            Email.sendEmail({
                                to: user.email,
                                subject: "Restore Password",
                                type: (user.type === 3) ? "business" : "graduate",
                                priority: 1,
                                html: `
                                    Hello ${(user.firstName || "there")}<br /><br />
                                    We have received a request to reset your password.<br /><br />
                                    Please click the link below (or right click and copy link into your browser) to create your new password:<br /><br />
                                    <a href="${keys.domainName + "restore/password/" + user._id + "/" + user.passwordRestoreHash}">Click Here</a>
                                `
                            }, function() {

                            })

                            return res.json({
                                success: true,
                                message: "We sent a link to your email address"
                            })

                        }

                    })

                })

            }

        })

})

router.post('/subscription/session', auth.required, (req, res, next) => {

    new Promise(function(resolve, reject) {

        let type = req.body.type;
        let email = req.body.email;
        let action = req.body.action;

        if (!action) {
            reject("Invalid action");
            return;
        }

        if (!email) {
            reject("Invalid email");
            return;
        }

        if (type === "contact-consent") {

            console.log("subscription manager - logged in", email, action);

            let id = Number(req.payload.bullhornId);

            if (action === "on") {

                console.log("Turning on consent")
                NoConsentUsers.deleteMany({
                    candidateId: id
                }).then(function(data) {
                    console.log(data);
                    console.log("---")
                }).catch(function(err) {
                    console.error(err);
                    console.log("---")
                })

                User.findOneAndUpdate({
                    bullhornId: id
                }, {
                    consentToBeContacted: (new Date())
                }).then(function(data) {
                    console.log(data);
                    CV.findOneAndUpdate({ candidateId: id }, { consented: false }).then(function() { console.log("CV update") }).catch(function(e) { console.log("CV update error", e) })
                    bullhornAPI.queryAPI(global.restUrl + "entity/Candidate/" + id + "?&BhRestToken=" + global.BhRestToken, 'post', {customText34: 'true'}).then(function(data) { console.log("CV update - bullhorn"); }).catch(function(err) { console.error("CV update error - bullhorn", err); })
                    resolve("Consent given")
                }).catch(function(err) {
                    console.error(err);
                    reject("Error giving consent")
                })

            } else {

                console.log("Turning off consent")
                let candidateConsent = new NoConsentUsers({ candidateId: id });
                candidateConsent
                    .save()
                    .then(function(data) {
                        console.log(data);
                    }).catch(function(err) {
                        console.error(err);
                    })

                User.findOneAndUpdate({
                    bullhornId: id
                }, {
                    consentToBeContacted: null
                }).then(function(data) {
                    console.log(data);
                    CV.findOneAndUpdate({ candidateId: id }, { consented: false }).then(function() { console.log("CV update") }).catch(function(e) { console.log("CV update error", e) })
                    bullhornAPI.queryAPI(global.restUrl + "entity/Candidate/" + id + "?&BhRestToken=" + global.BhRestToken, 'post', {customText34: 'false', customText27: 'unavailable'}).then(function(data) { console.log("CV update - bullhorn"); }).catch(function(err) { console.error("CV update error - bullhorn", err); })
                    Graduate.withdrawAllApplications(req.payload.bullhornId);
                    resolve("Consent revoked")
                }).catch(function(err) {
                    console.error(err);
                    reject("Error revoking consent")
                })

            }

            // bullhornAPI
            //     .queryAPI(global.restUrl + "search/Candidate?&fields=id,email&query=email:'" + email + "' &BhRestToken=" + global.BhRestToken, 'get')
            //     .then(function(data) {
            //         console.log(data);
            //     }).catch(function(err) {
            //         console.log(err);
            //     })

        } else {
            reject("Invalid type");
            return;
        }

    }).then(function(data) {
        res.json({
            success: true,
            data: data
        })
    }).catch(function(e) {
        console.log(e)
        res.json({
            success: false,
            error: e
        })
    })

})

router.post('/subscription/:token', (req, res, next) => {

    new Promise(function(resolve, reject) {

        console.log("Sumscription manager - not logged in", req.body);

        let type = req.body.type;
        let email = req.body.email;
        let action = req.body.action;

        if (!action) {
            reject("Invalid action");
            return;
        }

        if (!email) {
            reject("Invalid email");
            return;
        }

        if (type === "contact-consent") {

            console.log("subscription manager - logged in", email, action);

            bullhornAPI
                .queryAPI(global.restUrl + "search/Candidate?&fields=id,email&query=email:" + email + " &BhRestToken=" + global.BhRestToken, 'get')
                .then(function(data) {

                    console.log(data);

                    if (data.data) {

                        let id = data.data[0].id;
                        console.log("FOUND id: " + id);

                        if (action === "on") {

                            console.log("Turning on consent")
                            NoConsentUsers.deleteMany({
                                candidateId: id
                            }).then(function(data) {
                                console.log(data);
                                console.log("---")
                            }).catch(function(err) {
                                console.error(err);
                                console.log("---")
                            })

                            User.findOneAndUpdate({
                                bullhornId: id
                            }, {
                                consentToBeContacted: (new Date())
                            }).then(function(data) {
                                console.log(data);
                                CV.findOneAndUpdate({ candidateId: id }, { consented: false }).then(function() { console.log("CV update") }).catch(function(e) { console.log("CV update error", e) })
                                bullhornAPI.queryAPI(global.restUrl + "entity/Candidate/" + id + "?&BhRestToken=" + global.BhRestToken, 'post', {customText34: 'true'}).then(function(data) { console.log("CV update - bullhorn"); }).catch(function(err) { console.error("CV update error - bullhorn", err); })
                                resolve("Consent given")
                            }).catch(function(err) {
                                console.error(err);
                                reject("Error giving consent")
                            })

                        } else if (action === "off") {

                            console.log("Turning off consent")
                            let candidateConsent = new NoConsentUsers({ candidateId: id });
                            candidateConsent
                                .save()
                                .then(function(data) {
                                    console.log(data);
                                }).catch(function(err) {
                                    console.error(err);
                                })

                            User.findOneAndUpdate({
                                bullhornId: id
                            }, {
                                consentToBeContacted: null
                            }).then(function(data) {
                                console.log(data);
                                CV.findOneAndUpdate({ candidateId: id }, { consented: false }).then(function() { console.log("CV update") }).catch(function(e) { console.log("CV update error", e) })
                                bullhornAPI.queryAPI(global.restUrl + "entity/Candidate/" + id + "?&BhRestToken=" + global.BhRestToken, 'post', {customText34: 'false', customText27: 'hidden'}).then(function(data) { console.log("CV update - bullhorn"); }).catch(function(err) { console.error("CV update error - bullhorn", err); })
                                resolve("Consent revoked")
                            }).catch(function(err) {
                                console.error(err);
                                reject("Error revoking consent")
                            })

                        } else {

                            console.log("QUery: " + req.body.email);

                            NoConsentUsers.findOne({
                                candidateId: id
                            }).then(function(data) {
                                if (data) {
                                    console.log("NOT consented");
                                    resolve({
                                        consented: false
                                    })
                                } else {
                                    resolve({
                                        consented: true
                                    })
                                    console.log("Consented")
                                }
                            }).catch(function(err) {
                                console.error(err);
                                console.log("---")
                            })

                        }

                    } else {
                        reject("Invalid email address");
                    }
                }).catch(function(err) {
                    console.log(err);
                    reject(err);
                })

        } else {
            reject("Invalid type");
            return;
        }

    }).then(function(data) {
        res.json({
            success: true,
            data: data
        })
    }).catch(function(e) {
        console.log(e)
        res.json({
            success: false,
            error: e
        })
    })

})

router.post('/change-password', auth.required, (req, res, next) => {

    new Promise(function(resolve, reject) {

        let id = req.payload.id;
        let oldPassword = req.body.oldPassword;
        let newPassword = req.body.newPassword;
        let newPasswordAgain = req.body.newPasswordAgain;

        if (!id) {
            reject("Unexpected Error");
            return;
        }

        if (!oldPassword || !newPassword || !newPasswordAgain) {
            reject("All fields are required");
            return;
        }

        if (newPassword !== newPasswordAgain) {
            reject("The two passwords don't match");
            return;
        }

        if (newPassword.length < 6) {
            reject("The minimum password length is 6 characters");
            return;
        }

        User
            .findById({'_id': id})
            .exec(function(err, data) {

                if (!data.validatePassword(oldPassword)) {
                    reject("Old password is invalid");
                    return;
                }

                data.setPassword(newPassword);
                data.save(function(err, data) {

                    if (err) {
                        reject("Unexpected Error");
                        return;
                    } else {
                        resolve("Password updated")
                        return;
                    }

                })

            })

    }).then(function(data) {
        res.json({
            success: true,
            data: data
        })
    }).catch(function(e) {
        console.log(e)
        res.json({
            success: false,
            error: e
        })
    })

})

router.post('/restore/password', (req, res, next) => {

    const { body: { user } } = req;
    const terms = req.body.terms;

    if(!user.password) {
        return res.json({
            success: false,
            message: "Please enter the new password."
        })
    }

    if (user.password !== user.passwordAgain) {
        return res.json({
            success: false,
            message: "The two passwords don't match"
        })
    }

    if (user.password.length < 6) {
        return res.json({
            success: false,
            message: "The minimum password length is 6 characters"
        })
    }

    if ( (user.userId === undefined) || (user.hash === undefined) || (user.userId === null) || (user.hash === null) ) {
        return res.json({
            success: false,
            message: "Missing Credentials"
        })
    }

    User
        .find({'passwordRestoreHash': user.hash, '_id': user.userId}, "email passwordRestoreHash passwordRestoreExpiry bullhornId hash status")
        .exec(function(err, data) {

            if ( (err) || (data === undefined) || (data === null) || (data.length === 0) ) {

                return res.json({
                    success: false,
                    message: "Invalid Credentials"
                })

            } else {

                data = data[0];

                if (data.passwordRestoreExpiry < new Date()) {

                    return res.json({
                        success: false,
                        message: "Expired Credentials"
                    })

                }

                if ( (data.hash === null) || (data.hash === undefined) ) {
                    data.status = "Registered";
                }

                data.setPassword(user.password);
                data.passwordRestoreHash = data.passwordRestoreHash + "123";

                data.save(function(err, user) {

                    if (err) {

                        return res.json({
                            success: false,
                            message: "Unexpected Error"
                        })

                    } else {

                        return res.json({
                            success: true,
                            message: "Password has been upated"
                        })

                    }

                })

            }

        })

})

router.post('/email-signup', auth.optional, (req, res, next) => {

    let email = req.body.email;
    let token = req.body.token;

    console.log("EMAIL SIGNUP ", email, token);

    if ( (!token) || (!email) ) {
        return res.json({ success: false, error: "Missing credentials" })
    }

    if (!validator.isEmail(email)) {
        return res.json({ success: false, error: "Invalid email address" })
    }

    OneClickSignup.findOne({
        email: email,
        token: token,
    }).then(function(data) {

        if (!data) {
            return res.json({ success: false, error: "Invalid credentials" })
        }

        data.remove();

        bullhornAPI
            .queryAPI(global.restUrl + "search/Candidate?&fields=id,firstName,lastName&query=email:" + email + "&count=1" + "&BhRestToken=" + global.BhRestToken, 'get')
            .then(function(data) {

                console.log("Got bhurn", data.data);

                if (data.total !== 0) {

                    bullhornId = data.data[0].id;
                    firstName = data.data[0].firstName;
                    lastName = data.data[0].lastName;

                    User.findOne({
                        email: email
                    }).then(function(data) {

                        if (data) {
                            return res.json({ success: false, error: "You already have an account, please log in" })
                        }

                        let password = Utilities.randomStr(24, true);

                        let nowDate = new Date();

                        let user = new User({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            type: 4,
                            bullhornId: bullhornId,
                            termsAccepted: nowDate,
                            consentToBeContacted: nowDate,
                        })

                        user.setPassword(password);

                        user
                            .save()
                            .then(function(user) {

                                console.log("User saved", user)

                                Email.sendEmail({
                                    to: user.email,
                                    subject: "Welcome to GradBay",
                                    priority: 2,
                                    type: "graduate",
                                    html: `
                                        <strong>A user account has been created or modified</strong> <br /><br />

                                        User name: ${user.email} <br />
                                        Temporary password: ${password} <br /><br />

                                        Hi ${(user.firstName || "there")}, <br /><br />

                                        Welcome to GradBay, an exclusive marketplace for recent graduates to connect with growing businesses across all sectors, there are literally hundreds of jobs designed for grads at some of the world’s best-known brands, media agencies, tech start-ups, finance firms and lots more. <br /><br />
                                        Complete your profile to search and apply directly to employers you know are hiring graduates and even better they can message you about vacancies through GradBay messenger, giving you full control of the process. <br /><br />

                                        <strong>Here’s what to do next:</strong> <br />
                                        Sign in to update your password then add a profile picture, your CV and don’t forget to tag your profile with your skills and the types of jobs you’re interested in so graduate employers can find you.  <br /><br />

                                        <strong>Give your profile a boost: </strong> <br />
                                        Viewers retain 95% of a message when watching video compared to 10% when reading it in text. So why not reduce the time it takes to find a job by adding a video to your profile, choose from the top 5 most commonly asked screening questions, upload a video response and increase the chances of a graduate employer messaging you by over 73%...after all you’re more than just a paper CV!  <br /><br />

                                        <table style="text-align:center;margin: 0 auto;">
                                            <tr style="text-align:center;margin: 0 auto;">
                                                <td style="text-align:center;margin: 0 auto;">
                                                    <span class="es-button-border" style="margin:0 auto;text-align:center;border-style:solid;border-color:#3cf39b;background:#3cf39b;border-width:0px;display:inline-block;border-radius:30px;width:180px;">
                                                        <a href="${ keys.domainName + "login/" }" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:15px;color:#000;border-style:solid;border-color:#3cf39b;border-width:10px 20px;display:block;background:#3cf39b;border-radius:30px;font-weight:bold;font-style:normal;text-align:center;line-height:22px;text-align:center;width:180px;">
                                                            Sign in to GradBay
                                                        </a>
                                                    </span>
                                                </td>
                                            </tr>
                                        </table> <br /><br />

                                        Thanks and welcome to the Bay 😊 <br /><br />

                                    `
                                }, function() {

                                })


                                let profileLink = keys.domainName + "admin/login-as/" + user._id;

                                jobsDOM = "";

                                bullhornAPI
                                    .queryAPI(global.restUrl + "search/JobSubmission?fields=jobOrder&query=candidate.id:" + bullhornId + "&BhRestToken=" + global.BhRestToken, 'get')
                                    .then(function(data) {

                                        let jobs = data.data;
                                        for (let i = 0; i < jobs.length; i++) {
                                            let job = jobs[i].jobOrder;
                                            let link = keys.domain + "jobs/post/" + job.id;
                                            jobsDOM += "<a href='" + link + "'>" + job.title + " (" + job.id + ")</a> <br />";
                                        }

                                        // Sending email to Admins
                                        Slack.send("GRAD_SIGNUP",
                                            `
Email: ${user.email}
Profile: ${profileLink}
Name: ${firstName} ${lastName}
BullhornId: ${bullhornId}
Jobs: ${jobsDOM}
Source: Email
                                            `
                                        )

                                        Email.sendEmail({
                                            to: keys.adminEmailGraduate,
                                            subject: "Grad Signup",
                                            type: "admin",
                                            html: `
                                                Email: ${user.email}<br />
                                                Profile: ${profileLink}<br />
                                                Name: ${firstName} ${lastName}<br />
                                                BullhornId: ${bullhornId}<br />
                                                Jobs: ${jobsDOM}<br />
                                                Source: Email<br />
                                            `
                                        }, function() { })

                                    }).catch(function(err) {
                                        console.error("Failed to get applications", err);
                                    })

                                res.json({
                                    user: user.toAuthJSON(),
                                    success: true
                                })

                            }).catch(function(err) {
                                console.error("Couldn't save user", err);
                                return res.json({ success: false, error: "Invalid credentials" })
                            })

                        console.log("Now signing up", user);

                    }).catch(function(err) {
                        console.error("Couldn't find GradBay user", err);
                        return res.json({ success: false, error: "Invalid credentials" })
                    })

                }

            }).catch(function(err) {
                console.error("Couldn't find Bullhorn user", err);
                return res.json({ success: false, error: "Invalid credentials" })
            })

    }).catch(function(err) {
        console.error("Couldn't find Bullhorn user", err);
        return res.json({ success: false, error: "Invalid credentials" })
    })

});

router.post('/signup', auth.optional, (req, res, next) => {

    const { body: { user } } = req;
    const terms = req.body.terms;
    const termsContactConsent = req.body.termsContactConsent;

    console.log("user signup, consent to be contacted" + termsContactConsent);

    if ( (user.type !== "Graduate") && (user.type !== "Business") ) {
        return res.json({ success: false, error: "Unexpected type" })
    }

    if (user.type === "Graduate" && !req.body.source) {
        return res.json({ success: false, error: "Please enter where you heard about us." })
    }

    if (user.type === "Graduate" && req.body.source === "Pot Noodle Campaign" && !req.body.sourcePotNoodle) {
        return res.json({ success: false, error: "Please enter where you heard about us." })
    }

    if (user.type === "Graduate" && req.body.source === "Pot Noodle Campaign" && [
        'Aliyah Abbas',
        'Rebecca Louise McKenna',
        'Sarah-Jane Noke',
        'Charlie Quixley',
        'Laura Jones',
        'Cam Causer',
        'Ryan Chang',
        'Kavan Nijjer',
        'Fred Parker',
        'Lauren Owen',
        'Olivia Drysdale',
        'Jasmine Baragwanath',
        'Nina Watson',
        'Daniel Kikbel',
        'Charles Ryder',
        'Sophie Evans',
        'Sophie Tomkins',
        'Molly Locke',
        'Jessica Phillips',
        'Forrest',
        'Tom Farmilo',
        'Chris Lillie',
        'Aman Padan',
        'Yasmin Taylor',
        'Abby Wynne',
        'Jamie Kimber',
        'Nicole Whelan',
        'Boris Strumenliev',
        'Lisa Milanesio',
        'Millie Burge',
        'Aneeqa Siddiqui',
        'Angie Galdini',
        'Belle Blackburn',
        'Meg Jenkinson',
        'Shannon Seaton',
        'Eden Estrajch-Moffatt',
        'Sam Wash',
        'Ned Smith',
        'Sam Pougatch',
        'Emily Bacchus-Waterman',
        'Arwen Lewis-Anthony',
        'Edward Dickinson',
        'Maggie Woloski',
        'Grace Chambers',
        'Fern Gable',
        'Bethany E Bracken',
        'Georgia Shakeshaft',
    ].indexOf(req.body.sourcePotNoodle) === -1) {
        return res.json({ success: false, error: "Unknown Ambassador" })
    }

    if(!user.email) {
        return res.json({
            success: false,
            error: "Please enter an email address"
        })
    }

    user.email = user.email.toLowerCase();

    if(!user.fullName) {
        return res.json({
            success: false,
            error: "Please enter your name"
        })
    }

    if ( (!user.password) || (!user.passwordAgain) ) {
        return res.json({
            success: false,
            error: "Please enter a password"
        })
    }

    if (user.password !== user.passwordAgain) {
        return res.json({
            success: false,
            error: "The two passwords don't match"
        })
    }

    if (!terms) {
        return res.json({
            success: false,
            error: "You haven't accepted our terms and services"
        })
    }

    user.termsAccepted = new Date();

    if (!validator.isEmail(user.email)) {
        return res.json({
            success: false,
            error: "Please enter a valid email address"
        })
    }

    if (user.type === "Business") {

        user.type = 3;
        user.bullhornId = parseInt("100000" + (new Date().getTime()));

        if (!user.companyName) {
            return res.json({
                success: false,
                error: "Please enter your company's name"
            })
        }

        if (!user.tel) {
            return res.json({
                success: false,
                error: "Please enter your telephone number"
            })
        }

    } else {

        user.type = 4;

        if (!user.cv) {
            return res.json({
                success: false,
                error: "Please upload your CV"
            })
        }

        let ext = Utilities.getFileExtension(user.cvFileName);

        if (["docx", "txt", "doc", "pdf"].indexOf(ext.toLowerCase()) === -1) {
            return res.json({
                success: false,
                error: "Unsupported file format"
            })
        }

        if (termsContactConsent) {
            user.consentToBeContacted = new Date();
        } else {
            return res.json({
                success: false,
                error: "Please accept our consent checkbox."
            })
        }

    }

    user.firstName = user.fullName.split(' ').slice(0, -1).join(' ');
    user.lastName = user.fullName.split(' ').slice(-1).join(' ');
    delete user.fullName;

    new Promise(function(resolve, reject) {

        if (user.type === 3) {

            User
                .find({email: user.email}, "id")
                .then(function(data) {

                    return new Promise(function(innerResolve, innerReject) {

                        if (data.length === 0) {
                            innerResolve(user);
                        } else {
                            return res.json({
                                success: false,
                                error: "Email address is already taken"
                            })
                            innerReject();
                        }

                    })

                }).then(function(user) {

                    return new Promise(function(innerResolve, innerReject) {

                        bullhornAPI
                            .queryAPI(
                                global.restUrl + "search/ClientCorporation?fields=id,name&query=name:\"" + user.companyName + "\"&BhRestToken=" + global.BhRestToken,
                                'get',
                                {}
                            ).then(function(data) {

                                if (data.count > 0) {
                                    // user.bullhornCompanyId = data.data[0].id
                                    // innerResolve(user);
                                    return res.json({
                                        success: false,
                                        error: "This company is already set up, you can ask for an invitation from the administrator of this company"
                                    })
                                    innerReject();
                                } else {

                                    bullhornAPI
                                        .queryAPI(global.restUrl + "entity/ClientCorporation?&BhRestToken=" + global.BhRestToken, 'put', {
                                            "name": user.companyName,
                                            "status": "Active Account",
                                            "customText1": ( (keys.env === "PROD") ? "prod" : "dev" ),
                                            "customTextBlock1": JSON.stringify([
                                                {
                                                    name: "1st Round",
                                                    color: "#636680",
                                                    id: "1"
                                                },
                                                {
                                                    name: "2nd Round",
                                                    color: "#3732A0",
                                                    id: "2"
                                                },
                                                {
                                                    name: "Offer",
                                                    color: "#3cf39b",
                                                    id: "3"
                                                },
                                            ])
                                        })
                                        .then(async function(data) {

                                            user.bullhornCompanyId = data.changedEntityId;

                                            let newCompany = new Company({
                                                bullhornCompanyId: data.changedEntityId,
                                                purchases: [],
                                                jobCredits: [],
                                                cvDownloadCredit: 0,
                                                connectCredit: 0,
                                                connectHistory: [],
                                                cvHistory: [],
                                                companyName: user.companyName
                                            })

                                            let companyReference = await newCompany.save().catch(function(e) { console.error("Failed to create company", e); });

                                            if (!companyReference) {
                                                return innerReject("Failed to create company")
                                            }

                                            return innerResolve(user);

                                        })
                                        .catch(function(err) {
                                            console.log(err);
                                            innerReject(err);
                                        })

                                }

                            }).catch(function(err) {
                                innerReject(err);
                            })

                    })

                }).then(function(user) {

                    return new Promise(function(innerResolve, innerReject) {

                        bullhornAPI
                            .queryAPI(
                                global.restUrl + "entity/ClientContact?&BhRestToken=" + global.BhRestToken,
                                'put',
                                {
                                    clientCorporation: {
                                        "id": user.bullhornCompanyId
                                    },
                                    "email": user.email,
                                    "firstName": user.firstName,
                                    "lastName": user.lastName,
                                    "mobile": user.tel,
                                    "name": user.firstName + " " + user.lastName,
                                    "customText1": ( (keys.env === "PROD") ? "prod" : "dev" ),
                                    "customText2": "no"
                                }
                            )
                            .then(function(data) {
                                user.bullhornContactId = data.changedEntityId;
                                innerResolve(user);
                            })
                            .catch(function(err) {
                                console.log(err);
                                innerReject(err);
                            })

                    })

                })
                .then(function(user) {
                    resolve(user);
                })
                .catch(function(err) {
                    console.log(err);
                    reject(err);
                })

        } else {

            // Checking whether the candidate already exists on Bullhron
            bullhornAPI
                .queryAPI(global.restUrl + "search/Candidate?&fields=id,email&query=email:" + user.email + "&count=1" + "&BhRestToken=" + global.BhRestToken, 'get')
                .then(async function(data) {

                    if (data.total !== 0) {

                        // Candidate is already registered on Bullhorn
                        user.bullhornId = data.data[0].id;

                        await bullhornAPI.queryAPI(
                            global.restUrl + "entity/Candidate/" + user.bullhornId + "?&BhRestToken=" + global.BhRestToken,
                            'post',
                            {
                                customText29: req.body.source,
                                customText9: req.body.sourcePotNoodle,
                            }
                        ).catch(function(err) {
                            console.log(err, "Failed to save sources to existing")
                        })

                        User
                            .find({email: user.email}, "id")
                            .then(function(data) {

                                if (data.length === 0) {
                                    // Candidate is not yet in our datebase
                                    resolve(user);
                                } else {
                                    // Candidate is already in the database
                                    resolve(user);
                                }

                            })
                            .catch(function(err) {
                                console.log(err);
                                reject(err);
                            })

                    } else {

                        // Candidate is not found on Bullhorn so he doesn't have an account on GradBay either

                        // Sending candidate to Bullhorn

                        bullhornAPI
                            .queryAPI(global.restUrl + "entity/Candidate?&BhRestToken=" + global.BhRestToken, 'put', {
                                email: user.email,
                                firstName: user.firstName,
                                lastName: user.lastName,
                                name: user.firstName + " " + user.lastName,
                                status: "New Lead",
                                customText29: req.body.source,
                                customText9: req.body.sourcePotNoodle,
                                customText25: ( (keys.env === "PROD") ? "prod" : "dev" ),
                                customText12: "no", // Don't add to parse queue yet
                                customText13: "no", // Not already parsed
                            })
                            .then(function(data) {

                                user.bullhornId = data.changedEntityId;
                                resolve(user);

                            })
                            .catch(function(err) {
                                console.log(err);
                                reject(err)
                            })

                    }

                }).catch(function(err) {
                    console.log(err);
                    reject(err)
                })

        }

    }).then(function(newUser) {

        // return;

        const finalUser = new User(user);

        finalUser.setPassword(user.password);

        if (finalUser.type === 3) {
            console.log("Business user, now setting super admin rights");
            finalUser.role = "Super Admin";
            finalUser.fieldVisibility = [
                "Name",
                "Profile Picture",
                "Video",
                "Audio",
                "Education Stats",
                "Work Experience",
                "CV Download",
                "Cover Letter Download",
                "Portfolio Download"
            ];
            finalUser.privileges = [
                "loginAs",
                "editIntegrations",
                "manageUsers",
                "editBusinessDetails",
                "editATS",
                "broadcastApplicationChanges",
                "manageGradFieldVisibility",
                "addJob",
                "manageATS",
                "updateGradStatus",
                "sendMessages",
                "manageInterviewScheduler",
                "searchGrads",
                "viewAllJobs",
                "editAllJobs",
                "viewJobStatistics"
            ]
        }

        return finalUser
            .save()
            .then((userNew) => {

                let notificationEmail, notificationSubject, notificationContent;
                let notificationEmailUser, notificationSubjectUser, notificationContentUser;

                if (finalUser.type === 4) {

                    notificationEmail = keys.adminEmailGraduate
                    notificationSubject = "New Graduate sign up"
                    let profileLink = keys.domainName + "admin/login-as/" + userNew._id;
                    notificationContent =
                        `
Bullhorn Id: ${userNew.bullhornId}
Email: ${userNew.email}
Name: ${userNew.firstName} ${userNew.lastName}
                        `

                    Email.sendEmail({
                        to: keys.adminEmailGraduate,
                        subject: notificationSubject,
                        type: "admin",
                        html: `
                            Bullhorn Id: ${userNew.bullhornId}
                            Email: ${userNew.email}
                            Name: ${userNew.firstName} ${userNew.lastName}
                        `
                    }, function() { })

                    // notificationContentUser = `
                    //     Hi ${finalUser.firstName},<br/><br/>
                    //     Welcome to GradBay, we’re here to help you find a job 😊<br/><br/>
                    //     GradBay is completely free to you as a graduate, your profile is quick to complete and you have complete control over when it’s active or deleted. <br/><br/>
                    //     The GradBay platform gives you access to businesses looking to hire graduates. We’ve made it simple for both you and the employers to find what you’re looking for with an easy to use search engine helping you quickly combine search criteria of different locations, sectors, businesses, cultures and job types. With your user profile page you can express all your areas of interest, different skills and job requirements by controlling how you appear on our platform, allowing you to connect with business who share these aims with their open vacancy. Through your user profile you can see which skills and areas are most sought after by hiring businesses helping you position yourself better and with your Live Jobs you can check on the status of each of your applications.<br/><br/>
                    //     When you apply for a job the GradBay team will make sure the hiring manager receives this, communicate when you’ve secured an interview and will help manage the entire process. <br/><br/>
                    //     To complete your profile <a href="${keys.domainName}login">login</a> and visit My Profile, once saved your profile can be live within minutes and you’re one step closer to getting a great graduate job. <br/>
                    // `

                    // notificationContentUser = `
                    //     Hi ${finalUser.firstName},<br/><br/>
                    //     Welcome to GradBay. We’re here to help you find a graduate job with our easy to use platform.<br/><br/>
                    //     To get started, sign in and complete your profile page. Once live there are 2 ways you can find a job on GradBay.<br/><br/>
                    //     1. Search and apply to jobs directly, using your Profile Page to keep track of your applications.<br/>
                    //     2. The GradBay team contacting your directly about a vacancy we’re managing on an employer’s behalf.<br/><br/>
                    //     Watch out for further updates to the GradBay platform later this year, giving you more control over your job search by connecting you directly with grad employers and a Summer campaign which sees the GradBay platform be taken over by big brands to promote some really unusual, weird and wonderful jobs.<br/><br/>
                    // `;

                    notificationContentUser = `
                        Hi ${finalUser.firstName},<br/><br/>

                        Welcome to GradBay, an exclusive marketplace for graduates to connect with growing businesses across all sectors, there are literally hundreds of jobs designed for grads at some of the world’s best-known brands, media agencies, tech start-ups, finance firms and lots more. <br /><br />

                        Complete your profile to search and apply directly to employers you know are hiring graduates and even better they can message you about vacancies through GradBay messenger, giving you full control over your job hunt. <br /><br />

                        <strong>Here’s what to do next:</strong> <br />
                        Sign in, add a profile picture along with your CV and don’t forget to tag your profile with your skills and the types of jobs you’re interested in so graduate employers can find you.  <br /><br />

                        <strong>Give your profile a boost:</strong> <br />
                        Viewers retain 95% of a message when watching video compared to 10% when reading it in text. So why not reduce the time it takes to find a job by adding a video to your profile, choose from the top 5 most commonly asked screening questions, upload a video response and increase the chances of a graduate employer messaging you by over 73%...after all you’re more than just a paper CV!  <br /><br />

                        Thank you and welcome to the Bay 😊 <br /><br />
                    `;

                    // Upload CV to bullhorn, setting Daxtra to be parsed

                    let userId = finalUser.bullhornId;
                    let cvBase64 = user.cv.split(',')[1];
                    let fileName = (user.cvFileName || "");
                    let now = new Date();
                    now = now.getTime() + "GB";

                    Email.sendEmail({
                        to: keys.cvDropboxEmail,
                        subject: "New Grad CV (Signup)",
                        type: "admin",
                        html: `
                            Bullhorn Id: ${userId} <br/>
                        `,
                        attachments: [
                            {
                                path: user.cv
                            },
                        ]
                    }, function(e, a) { })

                    Slack.send("GRAD_UPLOAD",
                        `
Type: CV (attachment in Dropbox email)
Bullhorn Id: ${userId}
Email: ${user.email}
Name: ${user.firstName} ${user.lastName}
                        `
                    )

                    let externalId = userId + "-" + now;

                    bullhornAPI.queryAPI(
                        global.restUrl + "file/Candidate/" + userId + "?&BhRestToken=" + global.BhRestToken,
                        'put',
                        {
                            "fileType": "SAMPLE",
                            "externalID": externalId,
                            "name": fileName,
                            // "contentType": "application/pdf",
                            "description": "Uploaded from GradBay",
                            "type": "CV",
                            "fileContent": cvBase64,
                            "distribution": "internal",
                        }
                    ).then(function(data) {

                    }).catch(function(data) {
                        console.log(data);
                    })

                } else if (finalUser.type === 3) {

                    notificationEmail = keys.adminEmailBusiness
                    notificationSubject = "New Business sign up"
                    notificationContent = `
Bullhorn Id: ${finalUser.bullhornContactId}
Email: ${finalUser.email}
Name: ${finalUser.firstName} ${finalUser.lastName}
                    `

                    Email.sendEmail({
                        to: keys.adminEmailBusiness,
                        subject: notificationSubject,
                        type: "admin",
                        html: `
                            Bullhorn Id: ${finalUser.bullhornContactId}<br />
                            Email: ${finalUser.email}<br />
                            Name: ${finalUser.firstName} ${finalUser.lastName}<br />
                        `
                    }, function() { })

                    notificationContentUser = `
                        Hello ${finalUser.firstName},<br/><br/>
                        Welcome to GradBay. GradBay gives you instant access to an inclusive network of graduate talent. <br /><br />
                        We offer both a recruitment agency and a self managed service. What works best for you depends on how much time you have to manage the search and what you're looking to gain from using GradBay.<br /><br />
                        If you’re short on time or looking for hard to find candidates, the recruitment agency service is for you, if you simply want to access our network of graduates and manage the process yourself the self service option starts at £200 per job credit. There is more info on about each on our <a href="${keys.domainName}pricing">pricing page</a>.<br /><br />
                        To get started, go to our pricing page, select your service and start hiring today! <br /><br />
                        <table style="text-align:center;margin: 0 auto;">
                            <tr style="text-align:center;margin: 0 auto;">
                                <td style="text-align:center;margin: 0 auto;">
                                    <span class="es-button-border" style="margin:0 auto;text-align:center;border-style:solid;border-color:#3cf39b;background:#3cf39b;border-width:0px;display:inline-block;border-radius:30px;width:180px;">
                                        <a href="${ keys.domainName + "pricing" }" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:15px;color:#000;border-style:solid;border-color:#3cf39b;border-width:10px 20px;display:block;background:#3cf39b;border-radius:30px;font-weight:bold;font-style:normal;text-align:center;line-height:22px;text-align:center;width:180px;">
                                            Get started
                                        </a>
                                    </span>
                                </td>
                            </tr>
                        </table> <br /><br />
                    `

                }

                notificationEmailUser = finalUser.email;
                notificationSubjectUser = "Welcome to GradBay";

                // Sending email to Admins

                if (notificationSubject === "New Graduate sign up") {
                    Slack.send("GRAD_SIGNUP", notificationContent)
                } else {
                    Slack.send("BUSINESS_SIGNUP", notificationContent)
                }

                // Sending emails to new users
                Email.sendEmail({
                    to: notificationEmailUser,
                    subject: notificationSubjectUser,
                    html: notificationContentUser,
                    type: (finalUser.type === 3) ? "business" : "graduate",
                    priority: 2,
                }, function() {})

                res.json({
                    user: finalUser.toAuthJSON(),
                    success: true
                })

            }).catch((err) => {

                console.log(err);

                return res.json({
                    success: false,
                    error: "This email is already registered"
                })

            });

    }).catch(function(err) {

        console.log(err);

        return res.json({
            success: false,
            error: "Unexpected Error"
        })

    })

});

router.get('/whatsapp', auth.required, (req, res, next) => {

    new Promise(async function(resolve, reject) {

        let id = req.payload.id;

        let user = await User.findById({'_id': id}, "whatsAppAuthKey").catch(e => { console.error("Failed to get user", e) });

        if (user) {

            if (user.whatsAppAuthKey) {
                resolve(user.whatsAppAuthKey);
            } else {
                let key = Utilities.randomStr(5);
                await User.findOneAndUpdate({'_id': id}, { "whatsAppAuthKey": key }).catch(e => { console.error("Failed to update user", e) });
                resolve(key);
            }

        } else {
            reject();
        }

        console.log(user);
        resolve();

    }).then(function(data) {
        res.json({
            success: true,
            data: data
        })
    }).catch(function(e) {
        console.log(e)
        res.json({
            success: false,
            error: e
        })
    })

})

router.post('/login', auth.optional, (req, res, next) => {

    const { body: { user } } = req;

    if(!user.email) {
        return res.json({
            success: false,
            error: "Please enter an email address"
        })
    }

    user.email = user.email.toLowerCase();

    if(!user.password) {
        return res.json({
            success: false,
            error: "Please enter a password"
        })
    }

    return passport.authenticate('local', { session: false }, (err, passportUser, info) => {

        console.log(passportUser);

        if (err) {
            return next(err);
        }

        if (passportUser.locked) {
            return res.json({
                success: false,
                error: "Your account has been disabled."
            })
        }

        if (passportUser) {
            const user = passportUser;
            user.token = passportUser.generateJWT();
            return res.json({
                user: user.toAuthJSON(),
                onboardingCompleted: ( (passportUser.onboardingCompleted !== false) ) ? true : false,
                success: true
            });
        }

        return res.json({
            success: false,
            error: "We couldn't find your account with the provided info"
        })

    })(req, res, next);

});

router.post('/current', auth.required, (req, res, next) => {

    const { payload: { id } } = req;

    return User.findById(id)
    .then(async (user) => {

        if(!user) {
            return res.sendStatus(400);
        }

        let finalUser = user.toAuthJSON()
        finalUser.status = user.status;
        finalUser.wishlist = user.wishlist;
        finalUser.viewHistory = user.viewHistory;
        finalUser.interviews = user.interviews
        finalUser.applications = user.sentApplications.concat(user.pendingApplications);
        finalUser.consentToBeContacted = user.consentToBeContacted;
        finalUser.onboardingCompleted = ( (user.onboarding !== false) ) ? true : false;
        finalUser.locked = user.locked;
        finalUser.whatsAppAuthKey = user.whatsAppAuthKey;
        finalUser.whatsAppId = user.whatsAppId;
        finalUser.canMessage = true;
        finalUser.memberSince = user.createdAt;

        console.log("/current" + finalUser.whatsAppId + " _ " + finalUser.whatsAppAuthKey)

        let bullhornId
        let unreadMessageCount = 0;

        if (finalUser.bullhornContactId) {

            bullhornId = finalUser.bullhornContactId;
            finalUser.role = user.role;
            finalUser.privileges = user.privileges;
            finalUser.fieldVisibility = user.fieldVisibility;

            if (finalUser.role === "Super Admin") {
                finalUser.defaultVisibleFields = user.defaultVisibleFields;
                finalUser.defaultUserPrivileges = user.defaultUserPrivileges;
                finalUser.defaultAdminPrivileges = user.defaultAdminPrivileges;
            } else {

                let superAdmin = await Company.getSuperAdmin(req.payload.bullhornCompanyId);

                if (superAdmin) {
                    finalUser.defaultVisibleFields = superAdmin.defaultVisibleFields;
                    finalUser.defaultUserPrivileges = superAdmin.defaultUserPrivileges;
                    finalUser.defaultAdminPrivileges = superAdmin.defaultAdminPrivileges;
                }

            }

        } else {

            bullhornId = finalUser.bullhornId;

        }

        // Getting unread message count
        return MessageThreadModel
            .find({
                $or: [
                    {
                        userOne: bullhornId,
                        deletedByUserOne: false
                    },
                    {
                        userTwo: bullhornId,
                        deletedByUserTwo: false
                    }
                ]
            }, {
                'messages':{'$slice': [0, 1] }
            })
            .sort({ updatedAt: -1 })
            .then(async function(data) {

                for (let i = 0; i < data.length; i++) {
                    let lastThreadMessage = data[i].messages[0];
                    if (lastThreadMessage !== undefined) {
                        if ( (lastThreadMessage.sender !== bullhornId) && (!lastThreadMessage.seen) )  {
                            unreadMessageCount++;
                        }
                    }
                }

                finalUser.unreadMessageCount = unreadMessageCount

                let dateNow = new Date();

                if (user.type === 3) {

                    bullhornAPI.queryAPI(global.restUrl + "entity/ClientContact/" + bullhornId + "?fields=customText2,customDate3&BhRestToken=" + global.BhRestToken, 'get', { } ).then(function(data) {

                        if (
                            (data.data.customDate3 === null) ||
                            (data.data.customDate3 === undefined) ||
                            (new Date(data.data.customDate3).getDate() !== dateNow.getDate()) ||
                            (new Date(data.data.customDate3).getMonth() !== dateNow.getMonth())
                        ) {
                            bullhornAPI.queryAPI(
                                global.restUrl + "entity/ClientContact/" + bullhornId + "?fields=customDate3&BhRestToken=" + global.BhRestToken,
                                'post',
                                { customDate3: dateNow.getTime() }
                            ).then(function() {

                            })
                        } else {
                            console.log("No udpate")
                        }

                        return res.json({ user: finalUser });

                    })

                } else if (user.type === 4) {

                    const EmailAlertModel = require("../models/EmailAlert")

                    EmailAlertModel.find({
                        bullhornId: bullhornId,
                        isDeleted: false,
                        type: 'job'
                    }).then(function(data) {

                        finalUser.alerts = data;

                        bullhornAPI.queryAPI(
                            global.restUrl + "entity/Candidate/" + bullhornId + "?fields=customDate3,customText27&BhRestToken=" + global.BhRestToken,
                            'get')
                            .then(function(data) {

                                if (data.data.customText27) {
                                    finalUser.profileVisibility = data.data.customText27;
                                } else {
                                    finalUser.profileVisibility = "hidden"
                                }

                                res.json({ user: finalUser });

                                if (
                                    (data.data.customDate3 === null) ||
                                    (data.data.customDate3 === undefined) ||
                                    (new Date(data.data.customDate3).getDate() !== dateNow.getDate()) ||
                                    (new Date(data.data.customDate3).getMonth() !== dateNow.getMonth())
                                ) {
                                    bullhornAPI.queryAPI(
                                        global.restUrl + "entity/Candidate/" + bullhornId + "?fields=customDate3&BhRestToken=" + global.BhRestToken,
                                        'post',
                                        { customDate3: dateNow.getTime() }
                                    ).then(function() {

                                    })
                                } else {
                                    console.log("No udpate")
                                }
                            })

                    }).catch(function(err) {
                        console.log(err);
                    })

                } else {
                    finalUser.canMessage = false;
                    return res.json({ user: finalUser });
                }

            }).catch(function(err) {
                console.log(err)
            })

    });

});

router.post('/get-by-id', auth.required, (req, res, next) => {

    const { payload: { id } } = req;

    // Get our records
    return User.findById(id)
    .then((user) => {

        if (!user) {

            return res.sendStatus(400);

        } else {

            // No bullhorn id
            if (user.bullhornId === undefined) {

                res.json({
                    user: user
                });

            } else {

                Graduate
                    .getById({ id: user.bullhornId })
                    .then(function(bullhornUser) {

                        res.json({
                            user: user,
                            bhUser: bullhornUser.data[0]
                        });

                    }).catch(function(err) {

                        console.log(err);
                        return res.sendStatus(400);

                    })

            }


        }

    });

});

router.post('/account/delete-confirm', auth.required, auth.requiredCustom, (req, res, next) => {

    let userId = req.body.id;
    let hash = req.body.hash;

    if ( (Utilities.isENU(userId)) || (Utilities.isENU(hash)) ) {
        return res.json({
            success: false,
            message: "Missing credentials"
        })
    }

    User
        .find({
            'accountDeleteHash': hash,
            '$or': [
                {'bullhornId': userId},
                {'bullhornContactId': userId}
            ]
        })
        .exec(function(err, data) {

            console.log(err, data)

            if ( (err) || (data === undefined) || (data === null) || (data.length === 0) ) {

                return res.json({
                    success: false,
                    message: "Couldn't get account"
                })

            } else {

                data = data[0];

                if (data.accountDeleteExpiry < new Date()) {

                    return res.json({
                        success: false,
                        message: "Expired Credentials"
                    })

                }

                // Business
                if (data.type === 3) {

                    // new Promise(function(resolve, reject) {

                        User
                            //.findOne({
                            .findOneAndDelete({
                                'accountDeleteHash': hash,
                                '$or': [
                                    {'bullhornId': userId},
                                    {'bullhornContactId': userId}
                                ]
                            })
                            .then(function() {

                                bullhornAPI
                                .queryAPI(
                                    global.restUrl + 'search/JobOrder?fields=id,isOpen&query=isDeleted:false AND isOpen:true AND customText2:' + ( (keys.env === "PROD") ? "prod" : "dev" ) + ' AND clientContact.id:' + userId + '&BhRestToken=' + global.BhRestToken,
                                    'get'
                                ).then(function(data) {

                                    if (data.count > 0) {

                                        let promises = [];

                                        for (let i = 0; i < data.data.length; i++) {

                                            promises.push(new Promise(function(resolve, reject) {

                                                bullhornAPI
                                                .queryAPI(
                                                    global.restUrl + 'entity/JobOrder/' + data.data[i].id + '?BhRestToken=' + global.BhRestToken,
                                                    'post',
                                                    {
                                                        'isOpen': false
                                                    }
                                                ).then(function(data) { resolve() })
                                                .catch(function(data) { resolve() })

                                            }));

                                        }

                                        Promise.all(promises)
                                        .then(function(data){

                                            res.json({
                                                success: true,
                                            })

                                        })
                                        .catch(function(err){
                                            console.log(err);

                                            res.json({
                                                success: true,
                                            })

                                        });

                                    } else {

                                        res.json({
                                            success: true,
                                        })

                                    }

                                })

                            }).catch(function() {

                                return res.json({
                                    success: false,
                                    message: "Unexpected Error"
                                })

                        })

                    // })

                } else if (data.type === 4) {

                    const AWS = require('aws-sdk');
                    AWS.config.update({region: 'us-west-2'});

                    const s3 = new AWS.S3({
                        accessKeyId: keys.s3BucketAccessKey,
                        secretAccessKey: keys.s3BucketSecretAccessKey,
                    });

                    new Promise(function(resolve, reject) {

                        var params = {
                            Bucket: "gradbay",
                            Prefix: 'graduate/video-interviews/' + userId
                        };

                        s3.listObjects(params, function(err, data) {

                            if (err) return reject(err);

                            if (data.Contents.length == 0) resolve();

                            params = {Bucket: "gradbay"};
                            params.Delete = {Objects:[]};

                            data.Contents.forEach(function(content) {
                                params.Delete.Objects.push({Key: content.Key});
                            });

                            s3.deleteObjects(params, function(err, data) {
                                if (err) reject(err);
                                else resolve(data);
                            });

                        });

                    }).then(function(data) {

                        new Promise(function(resolve, reject) {

                            var params = {
                                Bucket: "gradbay",
                                Prefix: 'graduate/profile-pictures/' + userId
                            };

                            s3.listObjects(params, function(err, data) {

                                if (err) return reject(err);

                                if (data.Contents.length == 0) resolve();

                                params = {Bucket: "gradbay"};
                                params.Delete = {Objects:[]};

                                data.Contents.forEach(function(content) {
                                    params.Delete.Objects.push({Key: content.Key});
                                });

                                s3.deleteObjects(params, function(err, data) {
                                    if (err) reject(err);
                                    else resolve(data);
                                });

                            });

                        }).then(function(data) {

                            if (Utilities.isENU(userId)) {
                                return;
                            }

                            // let deleteURL = global.restUrl + "entity/Candidate/" + userId + "?BhRestToken=" + global.BhRestToken;
                            //
                            // bullhornAPI
                            // .queryAPI(
                            //     deleteURL,
                            //     'delete'
                            // ).then(function(data) {
                            //
                            //     console.log("DELETED BULLHORN PROFILE");

                                User
                                    .findOneAndDelete({'accountDeleteHash': hash, 'bullhornId': userId})
                                    .then(function() {

                                        Slack.send("DELETE_REQUEST",
                                            `
The following account has been deleted (you will have to delete the graduate manually from Bullhorn):
BullhornId: ${userId}
                                            `
                                        )

                                        Email.sendEmail({
                                            to: keys.adminEmailGraduate,
                                            subject: "Delete Request",
                                            type: "admin",
                                            html: `
                                                The following account has been deleted (you will have to delete the graduate manually from Bullhorn):  <br />
                                                BullhornId: ${userId} <br />
                                            `
                                        }, function() { })

                                        return res.json({
                                            success: true,
                                            message: "DELETED ALL DAtA-------------"
                                        })

                                    }).catch(function() {

                                        return res.json({
                                            success: false,
                                            message: "Unexpected Error"
                                        })

                                    })

                            // }).catch(function(err) {
                            //
                            //     console.log(err)
                            //
                            //     return res.json({
                            //         success: false,
                            //         message: "Unexpected Error"
                            //     })
                            //
                            // })

                        }).catch(function(err) {

                            console.log(err)

                            return res.json({
                                success: false,
                                message: "Unexpected Error"
                            })

                        })

                    }).catch(function(err) {

                        return res.json({
                            success: false,
                            message: "Unexpected Error"
                        })

                    })

                } else {

                }

            }

        }, function(err) {

            return res.json({
                success: false,
                message: "Unexpected Error"
            })

        })

})

router.post('/account/delete', auth.required, auth.requiredCustom, (req, res, next) => {

    let userId = req.payload.id;

    if (Utilities.isENU(req.body.password)) {

        return res.json({
            success: false,
            data: "Please enter a password"
        });

    }

    if (Utilities.isENU(userId)) {

        return res.json({
            success: false,
            data: "Invalid Credentials"
        });

    }

    User
    .findById({'_id': userId})
    .exec(function(err, data) {

        if ( (err) || (Utilities.isENU(data)) ) {

            return res.json({
                success: false,
                data: "Invalid Credentials"
            });

        } else {

            if (!data.validatePassword(req.body.password)) {

                return res.json({
                    success: false,
                    data: "Invalid password"
                });

            } else {

                crypto.randomBytes(48, function(err, buffer) {

                    let token = buffer.toString('hex');
                    data.accountDeleteHash = token;
                    data.accountDeleteExpiry = new Date(new Date().getTime() + 60000*24*7*1000);
                    data
                        .save()
                        .then(function(data) {

                            let deleteLink = keys.domainName + "dashboard/" + data.bullhornId + "/" + data.accountDeleteHash + "/delete"

                            if (data.type === 3) {
                                deleteLink = keys.domainName + "my-profile/business/" + data.bullhornContactId + "/" + data.accountDeleteHash + "/delete"
                            }

                            Slack.send("DELETE_REQUEST",
                                `
Account info:
Email: ${data.email}
First Name: ${data.firstName}
Last Name: ${data.lastName}
GradBay Id: ${data._id}
Bullhorn Id: ${( data.type === 4 ? data.bullhornId : data.bullhornContactId )}
Account Type: ${( data.type === 4 ? "Candidate" : "Business" )}
Delete account: ${deleteLink} (link expires in 7 days)
                                `
                            )

                            Email.sendEmail({
                                to: keys.adminEmailGraduate,
                                subject: "Delete Request",
                                type: "admin",
                                html: `
                                    Account info:<br />
                                    Email: ${data.email}<br />
                                    First Name: ${data.firstName}<br />
                                    Last Name: ${data.lastName}<br />
                                    GradBay Id: ${data._id}<br />
                                    Bullhorn Id: ${( data.type === 4 ? data.bullhornId : data.bullhornContactId )}<br />
                                    Account Type: ${( data.type === 4 ? "Candidate" : "Business" )}<br />
                                    Delete account: ${deleteLink} (link expires in 7 days)<br />
                                `
                            }, function() { })

                            return res.json({
                                success: true,
                                data: "We received your request to delete your account, we will review your request and notifiy you in 5 working days"
                            });

                        })
                        .catch(function(err) {

                            return res.json({
                                success: false,
                                data: "We couldn't process your request"
                            });

                        })

                });

            }

        }

    })

})

router.post('/wishlist/rewrite', auth.required, auth.requiredCustom, (req, res, next) => {

    User.rewriteWishlist({
        userId: req.payload.id,
        ids: req.body.ids
    }, function(err, data) {
        if (err) {
            console.log(err);
            res.sendStatus(400);
        } else {
            res.json({
                success: true
            });
        }
    })

})

router.post('/wishlist/add', auth.required, auth.requiredCustom, (req, res, next) => {

    User.addToWishlist({
        id: req.payload.id,
        jobId: req.body.id
    }, function(err, data) {

    })

})

router.post('/wishlist/remove', auth.required, auth.requiredCustom, (req, res, next) => {

    User.removeFromWishlist({
        id: req.payload.id,
        jobId: req.body.id
    }, function(err, data) {
        if (err) {
            res.sendStatus(400);
        } else {
            res.json({
                success: true
            });
        }
    })

})

router.post('/wishlist/remove-all', auth.required, auth.requiredCustom, (req, res, next) => {

    User.removeAllFromWishlist({
        id: req.payload.id,
    }, function(err, data) {
        if (err) {
            res.sendStatus(400);
        } else {
            res.json({
                success: true
            });
        }
    })

})

router.get('/allowed-to-message', auth.required, (req, res) => {

    User
        .isAllowedToMessage(req.payload)
        .then(function(data) {
            res.json({
                success: true,
                data: data
            })
        }).catch(function(e) {
            console.log(e);
            res.json({
                success: false,
                error: e
            })
        })

})

module.exports = router;
