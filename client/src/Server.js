import { Component } from 'react';
import axios from 'axios';

class Server {

    static logout() {
        localStorage.clear();
        window.location.href = "/";
    }

    static api(request) {

        let full = window.location.protocol+'//'+window.location.hostname+(window.location.port ? ':'+window.location.port: '');
        axios.defaults.baseURL = full;

        let headers = {

        }

        let authToken = localStorage.getItem('authToken');

        if (authToken) {
            headers['Authorization'] = "Token " + authToken
        }

        axios({
            method: request.method,
            url: request.url,
            data: request.data,
            headers: headers,
            params: request.params,
        }).then(function (response) {
            request.then(response);
        }.bind(this))
        .catch(function (error) {
            if (error && error.response && error.response.status === 401) {
                localStorage.clear();
                window.location.href = "/login"
            } else if (error && error.response && error.response.status === 429) {
                request.catch("Too many tries! Try again in 10 minutes");
            } else if (error && error.response && error.response.data && error.response.data.error) {
                request.catch(error.response.data.error);
            } else {
                request.catch("Unexpected Error");
            }
        }.bind(this));

    }

}

export default Server;
