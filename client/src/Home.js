import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './Home.css';
// import RedditIcon from '-!svg-react-loader!./icons/reddit.svg';

import Image1 from './images/1.jpg';
import Image2 from './images/2.jpg';
import Image3 from './images/3.jpg';
import Image4 from './images/4.jpg';
import Image5 from './images/5.jpg';

const SHOWCASE_IMAGES = [Image2, Image5, Image3, Image1, Image4]

class Home extends Component {

    componentDidMount() {

        document.title = window.globalVars.pageTitleBefore + "Home" + window.globalVars.pageTitleAfter;
        window.scrollTo({top: 0, left: 0, behavior: "smooth"});

        // Redirecting to main feed if logged in
        if (localStorage.getItem("authToken")) {
            window.location.href = "/dashboard/timeline"
        }

    }

    render() {

        return (
            <div className="home-page">
                <div className="main-container">
                    <aside>
                        <div className="feeds">
                            <div className="feed">
                                <label>Ideas</label>
                                    <div className="items">
                                        {[
                                            {
                                                title: "An app that holds sending texts until a certain time so you don’t forget what you wanted to say, but you don’t disrupt the sleep/work of people you want to tell.",
                                                body: "I live a very nocturnal life, and many of the people I love do not. I can’t send them my stream of consciousness comments as friends normally do because they’d get a bunch of texts at 4am.",
                                                match: true
                                            },
                                            {
                                                title: "Why can't smart watch makers make an app that makes your watch buzz every time you go to touch your face. It may help you infecting yourself from a contaminated surface.",
                                                body: "Yes it would only work for the hand the watch is on but at least it may make you more aware.",
                                                match: true
                                            },
                                            {
                                                title: "A pillow integrated with a speaker so you can comfortably fall asleep to an audiobook",
                                            },
                                            {
                                                title: "A Tinder style swiping app for musicians with audio samples that want to form bands or get discovered",
                                                body: "There would be categories for each different genre of music and the algorithm will set the most favorable artists first",
                                                match: true,
                                            },
                                            {
                                                title: "Someone design a toaster that releases crumbs instead of dispersing them evenly throughout the nooks and crannies.",
                                            },
                                            {
                                                title: "An app where we just watch ads and the app gives gives us a portion of the ad revenue.",
                                                match: true
                                            },
                                        ].map(item => {
                                            return (
                                                <div className="item" data-match={item.match}>
                                                    { (item.title) && <h3>{item.title}</h3> }
                                                    { (item.body) && <p>{item.body}</p> }
                                                    { (item.image) && <img src={item.image} /> }
                                                </div>
                                            )
                                        })}
                                    </div>
                            </div>
                            <div className="feed">
                                <label>Cute Dogs</label>
                                <div className="items">
                                    { SHOWCASE_IMAGES.map(item => {
                                        return (
                                            <div className="item image">
                                                <img src={item} />
                                            </div>
                                        )
                                    }) }
                                </div>
                            </div>
                            <div className="feed">
                                <label>MERN Jobs</label>
                                <div className="items">
                                    {[
                                        {
                                            title: "(Canada/Online) We are looking to commission a logo for our new business"
                                        },
                                        {
                                            title: "Python Mqtt reader plus Api Rest CRUD Mysql or MongoDB, 20$/hr",
                                            body: "Python, API/REST on AWS/Ubuntu Experience needed, must be able use mysql or Mongo DB.",
                                            match: true
                                        },
                                        {
                                            title: "(Online) Need someone to create my wordpress website for me.",
                                        },
                                        {
                                            title: "React front end developer for ongoing work",
                                            body: "I'm looking for a react dev to help with frontend development. It's part-time and I'll be giving work on-off based on how the project evolves.",
                                            match: true,
                                        },
                                        {
                                            title: "Illustrator for children’s book",
                                        },
                                        {
                                            title: "Drupal Developer"
                                        },
                                        {
                                            title: "ReactJS / NodeJS Developer to teach me Advanced Concepts via Skype",
                                            body: "Looking to learn some more advanced concepts - authentication, JWT, stripe & paypal integration, etc. Looking for an advanced dev who is willing to teach me these concepts regularly (once / twice a week) via Skype.",
                                            match: true
                                        },
                                        {
                                            title: "Remote React contractor",
                                            body: "I’m looking to hire a React contractor for 2-3 months to work on an already built web-based product. We are a company in the fitness and amenities space.",
                                            match: true
                                        },
                                    ].map(item => {
                                        return (
                                            <div className="item" data-match={item.match}>
                                                { (item.title) && <h3>{item.title}</h3> }
                                                { (item.body) && <p>{item.body}</p> }
                                                { (item.image) && <img src={item.image} /> }
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </aside>
                    <main>
                        <div className="header">
                            {/*<RedditIcon />*/}
                            <h1>Reddit Notifier</h1>
                        </div>
                        <div className="body">
                            <p>Reddit is an awesome place to get your latest memes, keep updated of latest news, get help, look for a job and for virtually anything else.</p>
                            <p>But there are just so many stuff that you can spend hours just scrolling aimlessly and Reddit’s search is like no search at all. Reddit Notifier allows you to set up advanced filters so you only see what you really want to see on your personalized feed.</p>
                            <p>You can also set up notifications sent to Slack or accessible via an RSS feed so you can keep in the loop on the go.</p>
                        </div>
                        <div className="options">
                            <Link to="/register" className="primary button">Get started</Link>
                            <Link to="/templates" className="button">Explore templates</Link>
                        </div>
                    </main>
                </div>
            </div>
        );

    }

}

export default Home;
