const mongoose = require('mongoose');
const router = require('express').Router();
const Post = require('../models/Post.js');
const Channel = require('../models/Channel.js');
const RSS = require('rss');

/**
* Generates an RSS feed for a channel
* @param {string} id id of the channel (param)
*/
router.get('/:id', (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let id = req.params.id;

        let channelDetails = await Channel.findOne({ _id: id }, "name");

        if (!channelDetails) {
            return reject({ code: 404, msg: "Not found" })
        }

        // Getting the last 100 posts
        posts = await Post
            .find({ channelId: id })
            .sort({ updatedAt: -1 })
            .limit(100)
            .catch(e => { console.error("Failed to get feed", e) });

        if (!posts) {
            return reject({ code: 500, msg: "Failed to get feed" });
        }

        let feed = new RSS({
            title: channelDetails.name,
            ttl: 30,
            image_url: 'https://redditnotifier.com/android-icon-144x144.png',
            feed_url: 'https://redditnotifier.com/rss/' + id,
            site_url: "https://redditnotifier.com",
        });

        // Feeding the items into the feed
        for (let i = 0; i < posts.length; i++) {

            let post = posts[i];

            let feedItem = {
                title: post.meta.title,
                description: post.meta.body ? post.meta.body : post.meta.title + " https://reddit.com" + post.meta.permalink,
                url: "https://reddit.com" + post.meta.permalink,
                guid: post.meta.permalink,
                date: post.meta.created,
            }

            if (post.meta.thumbnailMedium) {
                feedItem.enclosure = {
                    url: post.meta.thumbnailMedium.replace("https://", "http://"),
                    // file: post.meta.thumbnailMedium,
                    type: 'image/jpeg'
                }
                feedItem.description += " " + post.meta.thumbnailMedium;
            }

            feed.item(feedItem);

        }

        let xml = feed.xml({indent: true});
        return resolve(xml);

    }).then(function(data) {
        // Resolving results as xml
        res.set('Content-Type', 'text/xml');
        res.send(data);
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

module.exports = router;
