const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path')
const compression = require('compression');
const RateLimit = require('express-rate-limit');

const app = express();

app.use(compression());
const keys = require('./config/keys');

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));

const db = keys.mongoURL;

mongoose
    .connect(db)
    .then(() => console.log('Mongo Connected'))
    .catch(err => console.log(err));

// Rate limiting the user endpoint for 10 requests  / 10 minutes
app.use('/api/user', new RateLimit({
    windowMs: 600*1000,
    max: 10,
    delayMs: 0,
    onLimitReached: function() { console.log("LIMIT REACHED") }
}));

// Rate limiting all endpoints to 300 requests / 10 minutes
app.use('*', new RateLimit({
    windowMs: 600*1000,
    max: 300,
    delayMs: 0,
    onLimitReached: function() { console.log("LIMIT REACHED") }
}));

// Routes
app.use('/api/user', require('./routes/User'));
app.use('/api/channel', require('./routes/Channel'));
app.use('/api/feed', require('./routes/Feed'));
app.use('/api/post', require('./routes/Post'));
app.use('/api/template', require('./routes/Template'));
app.use('/rss', require('./routes/RSS'));

// Static route
app.use(express.static('client/build'));

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
})

// Crons
require('./crons/RedditCrawler');

const port = process.env.PORT || 8080;

process.on('uncaughtException', function(err) {
    console.log('Caught exception: ' + err);
});

app.listen(port, () => console.log(`Server started on port ${port}`))
