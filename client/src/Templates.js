import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Masonry from 'react-masonry-css'

import './Templates.css';
import Server from "./Server";
import ChannelEditorModal from "./components/ChannelEditorModal";
// import AddCircleIcon from '-!svg-react-loader!./icons/add-circle.svg';

class Templates extends Component {

    constructor(props) {
        super(props)
        this.state = {
            templates: []
        }
    }

    componentDidMount() {

        document.title = window.globalVars.pageTitleBefore + "Templates" + window.globalVars.pageTitleAfter;
        window.scrollTo({top: 0, left: 0, behavior: "smooth"});

        this.getTemplates();

    }

    getTemplates() {

        Server.api({
            method: "GET",
            url: "/api/template/",
            then: function(res) {
                if (res && res.data && Array.isArray(res.data)) {
                    this.setState({ templates: res.data })
                }
            }.bind(this),
            catch: function(errorMsg) {
                console.error("Failed to get temapltes");
            }.bind(this)
        })

    }

    render() {

        return (
            <div className="templates-page">
                <div className="main-container">
                    <div className="intro">
                        <h1>Template Collection</h1>
                        <p>Get inspired with the following templates demonstrating some advanced use cases. Let us know if you want to include something that is not in the list.</p>
                    </div>
                    <div className="templates">
                        <Masonry
                            breakpointCols={{
                                default: 3,
                                900: 2,
                                600: 1
                            }}
                            className="my-masonry-grid"
                            columnClassName="my-masonry-grid_column"
                        >
                            { this.state.templates.map(item => {
                                return (
                                    <div className="template" onClick={() =>{
                                        if (!localStorage.getItem("authToken")) {
                                            this.props.history.push("/register")
                                        } else {
                                            this.setState({
                                                channelEditorModalId: "new",
                                                channelEditorTemplate: item,
                                            })
                                        }
                                    }}>
                                        <h2>{item.name}</h2>
                                        <div className="subreddits">
                                            { item.subreddits.map(subreddit => {
                                                return (<span>{subreddit}</span>)
                                            }) }
                                        </div>
                                        { (item.querySQL !== "()") &&
                                            <p>All posts that {item.querySQL.replace(/post /g, '').replace(/()/g, '')}</p>
                                        }
                                    </div>
                                )
                            }) }
                        </Masonry>
                    </div>
                    <div className="contact">
                        <a className="primary button" href="mailto:hello@redditnotifier.com" target="_blank">Suggest something</a>
                    </div>
                </div>
                <ChannelEditorModal template={this.state.channelEditorTemplate} id={this.state.channelEditorModalId} toggle={(id) => {
                    this.setState({ channelEditorModalId: null })
                    if (id && typeof id === "string") {
                        window.location.href = "/dashboard/channel/" + id
                    }
                }} />
            </div>
        );

    }

}

export default Templates;
