import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './Terms.css';

class Terms extends Component {

    componentDidMount() {
        document.title = window.globalVars.pageTitleBefore + "Terms" + window.globalVars.pageTitleAfter;
        window.scrollTo({top: 0, left: 0, behavior: "smooth"});
    }

    render() {

        return (
            <div className="terms-page">
                <div className="main-container">

                    <ul className="links">
                        <li>Terms and Conditions (Last updated 11/04/2020)</li>
                        <li>Privacy Policy (Last updated 11/04/2020)</li>
                        <li>GDPR (Last updated 11/04/2020)</li>
                    </ul>

                    <h2>Changes to our Terms & Conditions and Privacy Policy</h2>
                    <p>We reserve the right to change our Terms & Conditions and Privacy Policy at any time.</p>
                    <p>Your continued use of Reddit Notifier after any modifications of our Terms & Conditions or Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide by them.</p>
                    <p>If we make changes to this page, we will notify you through the email address you have provided us.</p>
                    <p>Please read these Terms & Conditions & Privacy Policy carefully before using the <a href="https://redditnotifier.com/">redditnotifier.com</a> website and the Reddit Notifier mobile applications operated by Dominik Gyecsek ("Reddit Notifier", "us", "we", or "our").</p>
                    <p>Your access to and use of Reddit Notifier is conditioned upon your acceptance of and compliance with these Terms and Conditions and Privacy Policy. If you disagree with any part then you do not have permission to use our service.</p>
                    <p><strong>Contact us if you have any questions at <a href="mailto:hello@redditnotifier.com">hello@redditnotifier.com</a></strong></p>

                    <div id="terms" ref={this.termsRef}>

                        <h2>Terms & Conditions</h2>

                        <h3>Communications</h3>
                        <p>We will only send emails to your email address in the following cases:</p>
                        <ul>
                            <li>Terms & Condition, Privacy Policy changes</li>
                            <li>Respond to one of your queries</li>
                        </ul>

                        <h3>Content</h3>
                        <p>All aggregated posts stored on the Service will be stored indefinitely.</p>

                        <h3>Accounts</h3>
                        <p>You are responsible for maintaining the confidentiality of your account and password. You agree to accept responsibility for any and all activities or actions that occur under your account. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.</p>

                        <h3>Indemnification</h3>
                        <p>You agree to defend, indemnify and hold harmless Reddit Notifier from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses, resulting from or arising out of</p>
                        <ul>
                            <li>your use and access of the Service, by you or any person using your account and password</li>
                            <li>a breach of these Terms</li>
                            <li>Content posted on the Service.</li>
                        </ul>

                        <h3>Limitation Of Liability</h3>
                        <p>In no event shall Reddit Notifier be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from</p>
                        <ul>
                            <li>your access to or use of or inability to access or use the Service</li>
                            <li>any conduct or content of any third party on the Service</li>
                            <li>any content obtained from the Service</li>
                            <li>unauthorized access, use or alteration of your transmissions or content</li>
                        </ul>

                        <h3>Disclaimer</h3>
                        <p>Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied.</p>

                        <p>We do not warrant that</p>
                        <ul>
                            <li>the Service will function uninterrupted, secure or available at any particular time or location</li>
                            <li>any errors or defects will be corrected</li>
                            <li>the Service is free of any harmful components</li>
                            <li>the results of using the Service will meet your requirements</li>
                        </ul>

                    </div>

                    <div id="privacy" ref={this.privacyRef}>

                        <h2>Privacy Policy</h2>

                        <p>This page informs you of our policies regarding the collection, use and disclosure of Personal Information when you use our Service.</p>
                        <p>We will not use or share your information with anyone except as described in this Privacy Policy.</p>
                        <p>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy.</p>
                        <p>While using our Service, we may ask you to provide us with personally identifiable information, these may include, your email address and other personal information ("Personal Information").</p>
                        <p>We collect this information for the purpose of identifying and communicating with you.</p>

                        <h3>Log Data</h3>
                        <p>We collect information that your browser sends whenever you visit our website, use our API (RSS feed, Slack hooks).</p>
                        <p>This Log Data may include the following information:</p>
                        <ul>
                            <li>IP address</li>
                            <li>Browser type</li>
                            <li>Browser version</li>
                            <li>The pages of our Service that you visit and the time and date of your visit</li>
                            <li>Mobile device's type</li>
                            <li>Mobile device's unique ID</li>
                            <li>Mobile device's operating system</li>
                            <li>other statistics</li>
                        </ul>

                        <h3>Compliance With Laws</h3>
                        <p>We will disclose your Personal Information where required to do so by law or subpoena or if we believe that such action is necessary to comply with the law and the reasonable requests of law enforcement or to protect the security or integrity of our Service.</p>

                        <h3>Security</h3>
                        <p>The security of your Personal Information is important to us, and we strive to implement and maintain reasonable, commercially acceptable security procedures and practices appropriate to the nature of the information we store, in order to protect it from unauthorized access, destruction, use, modification, or disclosure.</p>
                        <p>However no method of transmission over the internet, or method of electronic storage is 100% secure and we are unable to guarantee the absolute security of the Personal Information we have collected from you.</p>

                        <h3>International Processing</h3>
                        <p>Your information, including Personal Information, may be transferred and stored on servers located outside of your governmental jurisdiction where the data protection laws may be different than those from your jurisdiction.</p>

                        <h3>Underage</h3>
                        <p>Our service is only available for people who are 18 or older.</p>
                        <p>We do not knowingly collect personally identifiable information from people under 18.</p>
                        <p>If we become aware that we have collected Personal Information from a person under 18 without verification of parental consent, we will remove that information from our service.</p>

                    </div>

                    <div id="gdpr" ref={this.gdprRef}>

                        <h2>GDPR</h2>

                        <h3>Data Export</h3>
                        <p>You can request a full data export containing all data related to your account. Once requested we will send you a download link in 5 business days.</p>

                        <h3>Deletion</h3>
                        <p>You can request to terminate your account with all your data deleted by contacting us.</p>

                    </div>

                </div>
            </div>
        );

    }

}

export default Terms;
