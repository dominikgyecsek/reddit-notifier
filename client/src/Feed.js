import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import './Feed.css';
import Server from "./Server";
import Post from "./components/Post";
import ChannelEditorModal from "./components/ChannelEditorModal";
import SlackHookModal from "./components/SlackHookModal";
import RSSFeedModal from "./components/RSSFeedModal";

import RedditIcon from '-!svg-react-loader!./icons/reddit.svg';
import DropdownIcon from '-!svg-react-loader!./icons/dropdown.svg';
import ReloadIcon from '-!svg-react-loader!./icons/reload.svg';

class Feed extends Component {

    constructor(props) {
        super(props)
        this.loading = false;
        this.allLoaded = false;
        this.state = {
            channelEditorModalId: null,
            slackHookModalId: null,
            type: null,
            id: null,
            details: null,
            keywords: "",
            page: 0,
            channelTitle: "All Channels",
            details: {
                name: "Hello"
            },
            feed: null,
            loading: false,
            noResults: false,
            lastReviewedDate: null,
        }
    }

    componentDidMount() {

        document.title = window.globalVars.pageTitleBefore + "Dashboard" + window.globalVars.pageTitleAfter;
        window.scrollTo({top: 0, left: 0, behavior: "smooth"});

        if (!localStorage.getItem("authToken")) {
            window.location.href = "/home"
        }

    }

    componentWillReceiveProps(nextProps) {

        // Loading another channel
        if (nextProps && nextProps.type && (nextProps.id !== this.state.id || nextProps.type !== this.state.type)) {
            this.loading = false;
            this.allLoaded = false;
            let lastReviewedDate = localStorage.getItem("lastReviewedDate_" + nextProps.type + "_" + nextProps.id);
            this.setState({
                id: nextProps.id,
                type: nextProps.type,
                page: 0,
                keywords: "",
                feed: null,
                loading: false,
                lastReviewedDate: lastReviewedDate ? parseInt(lastReviewedDate) : null
            }, function() {
                this.paginate();
            })
        }

    }

    reload() {

        // Reseting filters and reloading channels
        this.loading = false;
        this.allLoaded = false;
        this.setState({
            page: 0,
            keywords: "",
            feed: null,
            loading: false,
        }, function() {
            this.paginate();
        })

    }

    deleteChannel() {

        // Confirming a channel deletion
        if (window.confirm("Are you sure you want to delete this channel, this will remove all existing posts aggregated in this channel as well.")) {
            Server.api({
                method: "DELETE",
                url: "/api/channel/" + this.state.id,
                then: function(res) {
                    console.log("Channel deleted")
                    window.location.href = "/dashboard/timeline";
                }.bind(this),
                catch: function(errorMsg) {
                    console.log("Channel failed to delete", errorMsg)
                    window.location.href = "/dashboard/timeline";
                }.bind(this)
            })
        }

    }

    // Loading more results of a feed
    paginate() {

        if (this.state.type === "channel" || !this.state.id) {
            return;
        }

        console.log("PAGINATIONGN " + this.state.type + " " + this.state.id + " " + this.state.page)

        if (this.state.type === "timeline") {
            this.setState({ channelTitle: "All Channels" })
        } else if (this.state.type === "saved") {
            this.setState({ channelTitle: "Saved" })
        }

        this.loading = true;
        this.setState({ loading: true })
        // return;

        Server.api({
            method: "GET",
            url: "/api/feed/" + this.state.type + "/" + this.state.id,
            params: {
                page: this.state.page,
                keywords: this.state.keywords
            },
            then: function(res) {

                if (res && res.data && res.data.posts && Array.isArray(res.data.posts)) {

                    let existing = this.state.feed || [];

                    this.loading = false;
                    this.allLoaded = res.data.posts.length === 0,

                    this.setState({
                        feed: existing.concat(res.data.posts).filter((thing, index, self) =>
                            index === self.findIndex((t) => (
                                t._id === thing._id
                            ))
                        ),
                        allLoaded: res.data.posts.length === 0,
                        loading: false,
                        noResults: (existing.length + res.data.posts.length === 0)
                    })

                    console.log("OK FEED", res.data);

                }

                if (res && res.data && res.data.details) {

                    this.setState({
                        details: res.data.details,
                        channelTitle: res.data.details.name
                    })

                    localStorage.setItem("lastReviewedDate_" + this.state.type + "_" + this.state.id, Math.round(new Date().getTime()/1000))

                    console.log("OK details", res.data.details);

                }

            }.bind(this),
            catch: function(errorMsg) {
                console.error("Failed to get feed");
                this.loading = false;
                this.setState({ loading: false })
            }.bind(this)
        })

    }

    handleKeyPress = (e) => {

        if (e.key === 'Enter') {
            this.loading = false;
            this.allLoaded = false;
            this.setState({
                page: 0,
                feed: null,
                loading: false
            }, function() {
                this.paginate();
            })
        }

    }

    render() {

        return (
            <div className="feed-container" onScroll={(e) => {

                // Loading more results when the user reaches the end of the feed
                if ( (e.target.clientHeight + e.target.scrollTop + 50 > e.target.scrollHeight) && (!this.loading) && (!this.allLoaded) ) {
                     this.loading = true;
                     this.setState({
                         page: this.state.page + 1,
                         loading: false
                     }, function() {
                         this.paginate();
                     })
                }

            }}>
                { (this.state.channelTitle) &&
                    <header>
                        <h2>{this.state.channelTitle}</h2>
                        <div className="options">
                            <div className="keyword-search">
                                <input data-enabled={this.state.buttonEnabled} onKeyPress={this.handleKeyPress} onChange={(e) => {
                                    this.setState({ keywords: e.target.value })
                                }} defaultValue={this.state.keywords} type="keywords" name="keywords" id="keywords" placeholder="Search by keywords" />
                            </div>
                            <ReloadIcon onClick={() => { this.reload(); }} />
                            { (this.state.type && this.state.type !== "timeline" && this.state.type !== "saved") &&
                                <Dropdown isOpen={this.state.optionsDropdownOpen} toggle={() => { this.setState({ optionsDropdownOpen: !this.state.optionsDropdownOpen }) }}>
                                    <DropdownToggle>
                                        <DropdownIcon />
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem onClick={() => { this.setState({ channelEditorModalId: this.state.id }) }}>Edit Details</DropdownItem>
                                        <DropdownItem onClick={() => { this.deleteChannel()}}>Delete Channel</DropdownItem>
                                        <DropdownItem onClick={() => { this.setState({ slackHookModalId: this.state.id }) }}>Slack Notifications</DropdownItem>
                                        <DropdownItem onClick={() => { this.setState({ rssFeedModalId: this.state.id }) }}>RSS Feed</DropdownItem>
                                    </DropdownMenu>
                                </Dropdown>
                            }
                        </div>
                    </header>
                }
                { (this.state.feed) &&
                    <div className="posts">
                        { this.state.feed.map((post, i) => {
                            return (
                                <Post
                                    key={post._id}
                                    post={post}
                                    lastReviewedDate={this.state.lastReviewedDate}
                                    onDelete={() => {
                                        let feed = this.state.feed;
                                        feed.splice(i, 1);
                                        this.setState({ feed: feed })
                                    }}
                                />
                            )
                        }) }
                    </div>
                }
                <ChannelEditorModal id={this.state.channelEditorModalId} toggle={(id) => {
                    this.setState({ channelEditorModalId: null })
                }} />
                { (this.state.details) &&
                    <SlackHookModal id={this.state.slackHookModalId} url={this.state.details.slackUrl} toggle={() => { this.setState({ slackHookModalId: null }) }} />
                }
                { (this.state.details) &&
                    <RSSFeedModal id={this.state.rssFeedModalId} toggle={() => { this.setState({ rssFeedModalId: null }) }} />
                }
                { (this.state.loading && !this.state.feed) &&
                    <div className="fullscreen-spinner-container">
                        <svg className="spinner" viewBox="0 0 50 50">
                            <circle className="path" cx="25" cy="25" r="20" fill="none" strokeWidth="5"></circle>
                        </svg>
                    </div>
                }
                { (this.state.loading && this.state.feed) &&
                    <div className="spinner-container">
                        <svg className="spinner" viewBox="0 0 50 50">
                            <circle className="path" cx="25" cy="25" r="20" fill="none" strokeWidth="5"></circle>
                        </svg>
                    </div>
                }
                { (this.state.noResults) &&
                    <div className="no-results-container">
                        <div>
                            <h2>No results</h2>
                            <p>Results are aggragated every few minutes.</p>
                        </div>
                    </div>
                }
            </div>
        );

    }

}

export default Feed;
