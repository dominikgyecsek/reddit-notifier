const jwt = require('express-jwt');
let User = require('../models/User');
let keys = require('../config/keys');

const getTokenFromHeaders = (req) => {

    const { headers: { authorization } } = req;

    if(authorization && authorization.split(' ')[0] === 'Token') {
        return authorization.split(' ')[1];
    }
    return null;

};

const auth = {

    required: jwt({
        secret: keys.jwtSecret,
        userProperty: 'payload',
        getToken: getTokenFromHeaders,
    }),

    optional: jwt({
        secret: keys.jwtSecret,
        // userProperty: 'payload',
        // getToken: getTokenFromHeaders,
        credentialsRequired: false,
    })

}

module.exports = auth;
