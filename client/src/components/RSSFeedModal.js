import React, { Component } from 'react';
import { Modal, ModalHeader } from 'reactstrap';
import { WithContext as ReactTags } from 'react-tag-input';
import QueryBuilder, { formatQuery } from 'react-querybuilder';
import Server from '../Server';
import './RSSFeedModal.css';

class RSSFeedModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            id: null,
            name: "",
            buttonEnabled: true,
            slackUrl: ""
        };
    }

    componentWillReceiveProps(nextProps) {

        this.setState({ modal: nextProps.id ? true : false });

        if (nextProps.slackUrl) {
            this.setState({ slackUrl: nextProps.slackUrl })
        }

    }

    toggle = () => {
        this.props.toggle();
    }

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.save(e);
        }
    }

    disableButton = () => { this.setState({ buttonEnabled: false }) }
    enableButton = () => { this.setState({ buttonEnabled: true }) }

    onInputChange = (e) => {
        let modifiedField = e.target.id;
        let modifiedFieldValue = e.target.value;
        let state = {...this.state};
        state[modifiedField] = modifiedFieldValue;
        this.setState(state);
    }

    save = (e) => {

        if (e.target.getAttribute("data-enabled") !== "true") {
            return;
        }

        this.disableButton();

        Server.api({
            method: "PUT",
            url: "/api/channel/" + this.props.id + "/slackhookurl",
            data: {
                slackUrl: this.state.slackUrl
            },
            then: function(res) {
                console.log("SAVED!", res);
                this.toggle();
            }.bind(this),
            catch: function(errorMsg) {
                this.setState({ errorMsg: errorMsg })
                this.enableButton();
            }.bind(this)
        })

    }

    render() {

        return (
            <Modal isOpen={this.state.modal} toggle={this.toggle} className="rss-feed-modal">
                <ModalHeader toggle={this.toggle}>RSS</ModalHeader>
                <div className="body">
                    <div className="form-group">
                        <label>You can use the below URL to stay in the loop with your favorite RSS reader (e.g. <a href="https://feedly.com" target="_blank">Feedly.com</a>)</label>
                    </div>
                    <div className="form-group">
                        <label for="email">RSS Feed</label>
                        <input defaultValue={"https://redditnotifier.com/rss/" + this.props.id} type="text" name="rssUrl" id="rssUrl" />
                    </div>
                </div>
            </Modal>
        );

    }

}

export default RSSFeedModal;
