/**
 * QueryParser is used to handle operations with JSON-based nested-query rules
 */
class QueryParser {

    /**
    * Recursively extracts all rules of a nested query object into a flat array
    * @param {string} combinator The combinator of the rule set
    * @param {object} rules Object of nested rules in the rule set
    * @return {[object]} Flat array of rules
    */
    static recursiveExpand(combinator, rules) {

        let rulesArr = [];

        let ruleSetResults = [];
        let rootRuleSetResults = [];
        let rule;

        for (let i = 0; i < rules.length; i++) {

            rule = rules[i];

            if (rule.combinator) {
                rulesArr = rulesArr.concat(this.recursiveExpand(rule.combinator, rule.rules));
            } else {
                rulesArr.push(rule)
            }

        }

        return rulesArr;

    }

    /**
     * Generates a list of words from a given string that match the given query object
     * @param {object} query Object of nested rules in the rule set
     * @param {string} str String to match query against
     * @return {[string]} Array of matched words
     */
    static getMatchedKeywords(query, str) {

        let matches = [];

        let possibleValues = [];

        let rules = this.recursiveExpand(query.combinator, query.rules);
        for (let i = 0; i < rules.length; i++) {
            possibleValues.push(rules[i].value);
        }

        for (var i = 0; i < possibleValues.length; i++) {
            if (str.indexOf(possibleValues[i]) !== -1) {
                matches.push(possibleValues[i])
            }
        }

        return Array.from(new Set(matches));
    }

    /**
    * Returns whether the given root query object and string is match or not
    * @param {object} query Root object of nested rules in the rule set
    * @param {string} str String to match query against
    * @return {boolean} whether the given query object and string is a match or not
    */
    static compile(query, str) {
        // console.log("=========================COMPILE=========================")
        // console.log(str);
        return this.parseRuleSet(query.combinator, query.rules, str);
    }

    /**
    * Returns whether the given nested query object and string is match or not
    * @param {string} combinator The combinator of the rule set
    * @param {object} rules Object of nested rules in the rule set
    * @param {string} str String to match query against
    * @return {boolean} whether the given nested query object and string is a match or not
    */
    static parseRuleSet(combinator, rules, str) {

        // console.log("@@ parseRuleSet()")
        // console.log(combinator, rules)

        let ruleSetResults = [];
        let rootRuleSetResults = [];
        let rule;

        for (let i = 0; i < rules.length; i++) {

            rule = rules[i];

            if (rule.combinator) {
                rootRuleSetResults.push(this.parseRuleSet(rule.combinator, rule.rules, str));
            } else {
                ruleSetResults.push(this.parseRule(rule, str));
            }

        }

        let allIn = ruleSetResults.concat(rootRuleSetResults);
        ruleSetResults = allIn
        rootRuleSetResults = allIn

        if (rule.combinator) {

            // console.log("rootRuleSetResults: ", rootRuleSetResults);

            if (combinator === "and") {
                // console.log("__", rootRuleSetResults, "__")
                // console.log("1== " + this.isAllTrue(rootRuleSetResults))
                return this.isAllTrue(rootRuleSetResults);
            } else if (combinator === "or") {
                // console.log("2== " + this.isAnyTrue(rootRuleSetResults))
                return this.isAnyTrue(rootRuleSetResults);
            }

        } else {

            // console.log("ruleSetResults: ", ruleSetResults);

            if (combinator === "and") {
                // console.log("3== " + this.isAllTrue(ruleSetResults))
                return this.isAllTrue(ruleSetResults);
            } else if (combinator === "or") {
                // console.log("4== " + this.isAnyTrue(ruleSetResults))
                return this.isAnyTrue(ruleSetResults);
            }

        }

    }

    /**
    * Matches a single object rule against the given string
    * @param {object} rule Object of a single nested rule
    * @param {string} str String to match query against
    * @return {boolean} whether the given nested query object and string is a match or not
    */
    static parseRule(rule, str) {
        // console.log("@@ parseRule()")
        // console.log(rule)

        switch (rule.operator) {

            case "contains":
                if (!str || !rule.value) return false;
                // console.log("= " + (str.toLowerCase().indexOf(rule.value.toLowerCase()) > -1))
                return str.toLowerCase().indexOf(rule.value.toLowerCase()) > -1

            case "!contains":
                if (!str || !rule.value) return false;
                // console.log("= " + (str.toLowerCase().indexOf(rule.value.toLowerCase()) === -1))
                return str.toLowerCase().indexOf(rule.value.toLowerCase()) === -1

            default:
                console.error("Unknown operator")

        }

        return false

    }

    /**
    * Checks whether all results are a match (AND)
    * @param {[boolean]} rule match results
    * @return {boolean} whether all rules are true
    */
    static isAllTrue(arr) {
        for (let i = 0; i < arr.length; i++) {
            if (!arr[i]) {
                return false;
            }
        }
        return true;
    }

    /**
    * Checks whether any results are a match (OR)
    * @param {[boolean]} arr rule match results
    * @return {boolean} whether any rules are true
    */
    static isAnyTrue(arr) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]) {
                return true;
            }
        }
        return false;
    }

}

module.exports = QueryParser;
