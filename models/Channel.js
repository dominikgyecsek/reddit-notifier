const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChannelSchema = new Schema({
    name: String, // Name of the channel
    subreddits: [String], // Array of subreddit names to scan
    query: Schema.Types.Mixed, // Query object to match posts against
    userId: Schema.Types.ObjectId, // Id of the owner of the channel
    slackUrl: String, // Slack webhook url to send the notifications
    querySQL: String, // SQL-like formatted query string for easier understanding
}, {
	collection: 'channels',
	timestamps: true
});

module.exports = mongoose.model('Channel', ChannelSchema);
