const snoowrap = require('snoowrap');
const keys = require('../config/keys');
const request = require('request');

const QueryParser = require('../models/QueryParser');
const Channel = require('../models/Channel');
const Subreddit = require('../models/Subreddit');
const Post = require('../models/Post');

const r = new snoowrap(keys.redditAuth);

/**
 * Reddit API crawler using the snoowrap library
 */
class RedditCrawler {

    /**
    * Scans all subreddits entered by the user and assigns matching posts across all users
    */
    static async crawl() {

        return await (new Promise(async function(resolve, reject) {

            console.log("@@ Crawl start")

            // Getting all of the users' channels
            let channels = await Channel.find({}).catch(e => { console.error("Failed to get channels", e) });
            if (!channels) return null;

            // Getting the last 300 scanned fullnames (reddit id) of each subreddit
            let subredditHistory = await Subreddit.find({}, {parsedPosts: { $slice: -300 }}).catch(e => { console.error("Failed to get subreddits", e) });
            if (!subredditHistory) return null;

            let allSubreddits = [];

            for (let i = 0; i < channels.length; i++) {
                allSubreddits = allSubreddits.concat(channels[i].subreddits);
            }

            // Removed duplicate subreddit across channels and users
            allSubreddits = Array.from(new Set(allSubreddits))

            // Creating an object of already parses reddit posts
            let subredditsLatestPostParses = {};

            for (let i = 0; i < subredditHistory.length; i++) {
                subredditsLatestPostParses[subredditHistory[i].name] = subredditHistory[i].parsedPosts
            }

            console.log(allSubreddits);

            // Varriable storing all new posts before evaluation
            let posts = {};

            // Going over all subreddits
            for (let i = 0; i < allSubreddits.length; i++) {

                let subredditName = allSubreddits[i]

                // Getting the last 50 posts of each subreddit. Storing the last ids and using after/before will not work if any of these reference posts get deleted
                // as the API will return nothing making this method stuck and not returning any new posts, so we must always get all the latest posts of each subreddit
                // and match all these ids against our already parsed list of post ids (subredditHistory)
                posts[subredditName] = await r.getSubreddit(subredditName).getNew({limit: 50}).catch(e => { console.error("Failed to get subreddti", e) });
                posts[subredditName].sort((a, b) => { return a.created < b.created ? 1 : -1 })

                if (posts[subredditName]) {

                    let latestPostParses = subredditsLatestPostParses[subredditName] || [];
                    let newPostParses = [];

                    // Going over all new posts, determining whether each post has been proccessed already or not, compiling our meta object and removing unnecessary field
                    for (let j = 0; j < posts[subredditName].length; j++) {

                        let item = posts[subredditName][j];

                        // If the post has not been parsed yet by our server
                        if (latestPostParses.indexOf(item.name) === -1) {

                            // Storing post id to the newly parsed posts
                            newPostParses.push(item.name);

                            let video = null;
                            let videoThumbnail = null;
                            let image = null;
                            let thumbnailMedium = null;
                            let thumbnailLarge = null;

                            // Extracting media
                            if (item.media) {
                                if (item.media.reddit_video && item.media.reddit_video.fallback_url) {
                                    video = item.media.reddit_video.fallback_url
                                } else if (item.media.type === "youtube.com") {
                                    video = item.media.oembed.html
                                    videoThumbnail = item.media.oembed.thumbnail_url
                                } else if (item.media.type === "gfycat.com") {
                                    video = item.media.oembed.html
                                    videoThumbnail = item.media.oembed.thumbnail_url
                                } else if (item.media.type === "streamable.com") {
                                    video = item.media.oembed.html
                                    videoThumbnail = item.media.oembed.thumbnail_url
                                }
                            }

                            // extracting image
                            if (item.url.indexOf("gfycat.com") !== -1 || item.url.indexOf(".jpg") !== -1 || item.url.indexOf(".png") !== -1 || item.url.indexOf(".jpeg") !== -1) {
                                image = item.url;
                            }

                            // Getting thumbnial with 320px width
                            if (item.preview && item.preview.images && item.preview.images[0] && item.preview.images[0].resolutions && item.preview.images[0].resolutions.filter(item => item.width === 320)[0] ) {
                                thumbnailMedium = item.preview.images[0].resolutions.filter(item => item.width === 320)[0].url
                            }

                            // Getting thumbnial with 640px width
                            if (item.preview && item.preview.images && item.preview.images[0] && item.preview.images[0].resolutions && item.preview.images[0].resolutions.filter(item => item.width === 640)[0] ) {
                                thumbnailLarge = item.preview.images[0].resolutions.filter(item => item.width === 640)[0].url
                            }

                            // Final post object to be stored in our database
                            posts[subredditName][j] = {
                                body: item.selftext,
                                author: item.author.name,
                                title: item.title,
                                created: new Date(item.created_utc * 1000),
                                permalink: item.permalink,
                                video: video,
                                videoThumbnail: videoThumbnail,
                                thumbnail: item.thumbnail,
                                thumbnailMedium: thumbnailMedium,
                                thumbnailLarge: thumbnailLarge,
                                image: image,
                                // id: item.name
                            }

                        } else {
                            // Already parsed posts
                            posts[subredditName][j] = null;
                        }

                    }

                    // Removing null values (already parsed posts)
                    posts[subredditName] = posts[subredditName].filter(x=>x);

                    console.log(subredditName + " - " + newPostParses.length, newPostParses);

                    if (newPostParses.length !== 0) {
                        let doesSubredditRefExists = await Subreddit.find({ name: subredditName }).catch(e => { console.error("Failed to remove old subrefs", e) });
                        // Creating subreddit history reference if this is the first time the subreddit is crawled
                        if (!doesSubredditRefExists || doesSubredditRefExists.length === 0) {
                            // console.log("Does not exists yet subreddit ref last posts")
                            let doesSubredditRef = new Subreddit({
                                name: subredditName,
                                parsedPosts: [],
                            })
                            await doesSubredditRef.save().catch(e => { console.error("Failed to save subredref", e) });
                        }
                        // Pushing newly parsed posts into the subreddit history to be used in the next run
                        await Subreddit.updateOne({ name: subredditName }, {$push: { parsedPosts: { $each: newPostParses } }}).catch(e => { console.error("Failed to insert new parses", e) });
                    }

                }

            }

            // Going over all of the user's channels and inserting new matching posts
            for (let i = 0; i < channels.length; i++) {

                let channel = channels[i];
                let newPosts = [];

                // Going over all of the subreddits of a given channel
                for (let j = 0; j < channel.subreddits.length; j++) {

                    let subredditName = channel.subreddits[j];
                    // New posts
                    let postsInSubreddit = posts[subredditName]
                    // Sorting so the older posts get processed first so the user will see the newest ones in the feed
                    postsInSubreddit = postsInSubreddit.reverse();

                    // Going over all of the posts of a given channel's subreddits
                    for (let k = 0; k < postsInSubreddit.length; k++) {

                        let post = postsInSubreddit[k];

                        // The post is a match if there are no query rules defined for the channel or the post's content matches the channel's query rules
                        if ( (channel.query && channel.query.rules && channel.query.rules.length === 0) || (QueryParser.compile(channel.query, post.title + " " + post.body)) ) {

                            let links = [];

                            let matches = post.body.match(new RegExp(/\[(.*?)\]\((.*?)\)/, 'gm'));

                            // Extracting all urls from the post and escaping injection
                            if (matches && Array.isArray(matches)) {
                                for (let k = 0; k < matches.length; k++) {
                                    let name = matches[k].match(new RegExp(/\[(.*?)\]/, 'gm'))
                                    if (name && name[0]) {
                                        name = name[0];
                                        name = name.substr(1, name.length-2);
                                    }
                                    let url = matches[k].match(new RegExp(/\((.*?)\)/, 'gm'))
                                    if (url && url[0]) {
                                        url = url[0];
                                        url = url.substr(1, url.length-2);
                                    }
                                    if (url && name) {

                                        if (url.indexOf("javascript:") === -1) {
                                            links.push({
                                                url: url,
                                                name: name,
                                            })
                                        }

                                    }
                                }
                            }

                            // New post object
                            let newMatchedPost = new Post({
                                userId: channel.userId,
                                channelId: channel._id,
                                isBookmarked: false,
                                isDeleted: false,
                                meta: post,
                                links: links,
                                searchStr: post.title.toLowerCase() + " " + post.body.toLowerCase(),
                                matchedWords: (channel.query && channel.query.rules && channel.query.rules.length === 0) ? [] : QueryParser.getMatchedKeywords(channel.query, post.title.toLowerCase() + " " + post.body.toLowerCase())
                            })

                            // Sending notification to a slack channel if the channel has the hook url defined
                            if (channel.slackUrl) {

                                let message = "";
                                if (post.title) message += post.title + " "
                                if (post.body) message += post.body + " "
                                if (post.permalink) message += "https://reddit.com" + post.permalink + " "

                                if (post.video) {
                                    message += post.video
                                } else if (post.thumbnailMedium) {
                                    message += post.thumbnailMedium
                                } else if (post.thumbnailLarge) {
                                    message += post.thumbnailLarge
                                } else if (post.image) {
                                    message += post.image
                                }

                                if (message) {
                                    request.post(channel.slackUrl, { json: { text: message } }, function (error, response, body) {
                                        if (!error && response.statusCode == 200) {
                                            // console.log("Slack sent")
                                        } else {
                                            console.error("Slack error", error)
                                        }
                                    })
                                }

                            }

                            // Saving the post to the database
                            await newMatchedPost.save().catch(e => { console.error("Failed to save post", e) })

                        }

                    }

                }

            }

            console.log("@@ Crawl END")

            return resolve();

        }).catch(function(e) {
            console.error("Reddit error", e)
        }))

    }

}

module.exports = RedditCrawler;
