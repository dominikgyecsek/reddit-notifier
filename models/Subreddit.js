const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SubredditSchema = new Schema({
    name: String, // Name of the subreddit
    parsedPosts: [String], // Array of already parsed post ids
}, {
	collection: 'subreddits',
	timestamps: true
});

module.exports = mongoose.model('Subreddit', SubredditSchema);
