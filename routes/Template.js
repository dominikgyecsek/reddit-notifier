const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Template = require('../models/Template.js');

/**
* @return {[object]} list of all sample templates
*/
router.get('/', (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        let posts = await Template.find({}).catch(e => { console.error("Failed to get feed", e) });

        if (!posts) {
            return reject({ code: 500, msg: "Failed to get templates" });
        }

        return resolve(posts);

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

module.exports = router;
