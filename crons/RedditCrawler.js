const RedditCrawler = require('../models/RedditCrawler');
const cron = require('node-cron');

let isCrawlingNow = false;

// Running cron every 3 minutes
cron.schedule('*/3 * * * *', async () => {
// cron.schedule('*/10 * * * *', async () => {

    if (isCrawlingNow) {
        return;
    }

    isCrawlingNow = true;
    await RedditCrawler.crawl();
    isCrawlingNow = false;

})

// RedditCrawler.crawl();
