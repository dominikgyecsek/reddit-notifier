import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './Dashboard.css';
import Server from "./Server";
import Feed from "./Feed";
import ChannelEditorModal from "./components/ChannelEditorModal";
import RedditIcon from '-!svg-react-loader!./icons/reddit.svg';
import AddCircleIcon from '-!svg-react-loader!./icons/add-circle.svg';

class Dashboard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            channelEditorModalId: null,
            channels: []
        }
    }

    componentDidMount() {

        document.title = window.globalVars.pageTitleBefore + "Dashboard" + window.globalVars.pageTitleAfter;
        window.scrollTo({top: 0, left: 0, behavior: "smooth"});

        if (!localStorage.getItem("authToken")) {
            this.props.history.push("/home")
        }

        setTimeout(function() {
            this.reloadChannels();
        }.bind(this), 100)

    }

    reloadChannels() {

        // Getting channel list
        Server.api({
            method: "GET",
            url: "/api/channel/",
            then: function(res) {
                if (res && res.data && Array.isArray(res.data)) {
                    this.setState({ channels: res.data })
                    window.globalVars.channelNames = {};
                    res.data.map(item => {
                        window.globalVars.channelNames[item._id] = item.name
                        return item;
                    })
                }
            }.bind(this),
            catch: function(errorMsg) {
                console.error("Failed to get channels", errorMsg);
            }.bind(this)
        })

    }

    render() {

        return (
            <div className="dashboard-page">
                <div className="main-container">
                    <aside>
                        <div className="item" onClick={() => { this.props.history.push("/dashboard/timeline") }}>
                            <span>All Channels</span>
                        </div>
                        <div className="item" onClick={() => { this.props.history.push("/dashboard/saved") }}>
                            <span>Saved Posts</span>
                        </div>
                        <div className="item">
                            <span>Channels</span>
                            <AddCircleIcon onClick={() => { this.setState({ channelEditorModalId: "new" }) }} />
                        </div>
                        { this.state.channels.map(channel => {
                            return (
                                <div className="sub-item" onClick={() => { this.props.history.push("/dashboard/channel/" + channel._id) }}>
                                    <span>{channel.name}</span>
                                </div>
                            )
                        }) }
                    </aside>
                    <Feed type={this.props.match.params.type} id={this.props.match.params.id} />
                </div>
                <ChannelEditorModal id={this.state.channelEditorModalId} toggle={(id) => {
                    this.setState({ channelEditorModalId: null })
                    if (id && typeof id === "string") {
                        window.location.href = "/dashboard/channel/" + id
                    }
                }} />
            </div>
        );

    }

}

export default Dashboard;
