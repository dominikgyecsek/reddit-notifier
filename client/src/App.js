import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import Navbar from './components/Navbar.js';
import Home from './Home.js';
import Dashboard from './Dashboard.js';
import About from './About.js';
import Login from './Login.js';
import Terms from './Terms.js';
import Register from './Register.js';
import Templates from './Templates.js';
import Server from './Server';

import { increment, decrement } from './actions'

import './Main.css';

import { useDispatch, useSelector } from 'react-redux';

function App() {

    // constructor(props) {

    //     super(props);

    //     this.state = {
    //         user: null
    //     }

    // }

    // componentDidMount() {

    // }

    // componentWillMount() {

    //     window.globalVars = {
    //         pageTitleBefore: "",
    //         pageTitleAfter: " | Reddit Notifier"
    //     }

    //     setTimeout(function() {

    //         // Attempt to refresh auth token
    //         if (localStorage.getItem("authToken")) {

    //             Server.api({
    //                 method: "get",
    //                 url: "/api/user/current",
    //                 then: async function(res) {
    //                     if (res.data && res.data.user && res.data.user.token) {
    //                         localStorage.setItem("authToken", res.data.user.token)
    //                     }
    //                 }.bind(this),
    //                 catch: function(code, error) { console.log("token get error", error); }.bind(this)
    //             })

    //         }

    //     }.bind(this), 5000);

    //     setInterval(async function() {

    //         // Updating new url in the DOM to apply styles
    //         if (window.location.href !== this.state.currentUrl) {
    //             let newUrl = window.location.href;
    //             this.setState({ currentUrl: window.location.href })
    //             let html = document.querySelector("html");
    //             if (html) {
    //                 html.setAttribute("data-page", newUrl)
    //             }

    //         }

    //     }.bind(this), 100);

    // }

        const counter = useSelector(state => state.counter)
        const dispatch = useDispatch();
        return (
            <div >
                <BrowserRouter>
                    <div style={{ background: '#333' }}>
                        {/* <Navbar /> */}
                        <button onClick={() => dispatch(increment()) } >HERE {counter} </button>
                        {/* <Switch>
                            <Route path="/" render={() => <Redirect to="/home" />} exact={true} />
                            <Route path="/home" component={ Home } exact></Route>
                            <Route path="/templates" component={ Templates } exact></Route>
                            <Route path="/dashboard" component={ Dashboard } exact></Route>
                            <Route path="/dashboard/:type" component={ Dashboard } exact></Route>
                            <Route path="/dashboard/:type/:id" component={ Dashboard } exact></Route>
                            <Route path="/login" component={ Login } exact></Route>
                            <Route path="/terms" component={ Terms } exact></Route>
                            <Route path="/register" component={ Register } exact></Route>
                            <Route path="/about" component={ About } exact></Route>
                        </Switch> */}
                    </div>
                </BrowserRouter>
            </div>
        );

}

export default App;
