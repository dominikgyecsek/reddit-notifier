import React, { Component } from 'react';
import { Modal, ModalHeader } from 'reactstrap';
import { WithContext as ReactTags } from 'react-tag-input';
import QueryBuilder, { formatQuery } from 'react-querybuilder';
import Server from '../Server';
import './ChannelEditorModal.css';

// import CloseIcon from '-!svg-react-loader!../icons/close.svg';

class ChannelEditorModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            id: null,
            name: "",
            subreddits: [],
            query: null,
            querySQL: null,
            buttonEnabled: true,
            errorMsg: null,
        };
    }

    logQuery = (query) => {
        this.setState({
            query: query,
            querySQL: formatQuery(query, 'sql').replace(/like/g, 'contains').replace(/%/g, '').replace(/!contains/g, 'does not contain')
        })
    }

    componentWillReceiveProps(nextProps) {

        this.setState({
            modal: nextProps.id ? true : false,
        });

        if (nextProps.id !== this.state.id) {

            this.setState({
                id: nextProps.id
            })

            if (nextProps.id !== "new" && nextProps.id) {
                this.get(nextProps.id);
            }

            this.enableButton();

        }

        if (nextProps.template) {
            this.setState({
                name: nextProps.template.name,
                subreddits: nextProps.template.subreddits.map(item => { return { text: item, id: item } }),
                query: nextProps.template.query,
                querySQL: nextProps.template.querySQL
            })
        }

    }

    toggle = (id) => {
        // this.setState({
        //     modal: !this.state.modal
        // });
        this.props.toggle(id);
    }

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.signup(e);
        }
    }

    disableButton = () => { this.setState({ buttonEnabled: false }) }
    enableButton = () => { this.setState({ buttonEnabled: true }) }

    onInputChange = (e) => {
        let modifiedField = e.target.id;
        let modifiedFieldValue = e.target.value;
        let state = {...this.state};
        state[modifiedField] = modifiedFieldValue;
        this.setState(state);
    }

    get = (id) => {

        Server.api({
            method: "GET",
            url: "/api/channel/" + id ,
            then: function(res) {
                console.log("GOT IT!", res);
                if (res && res.data && Array.isArray(res.data.subreddits)) {
                    res.data.subreddits = res.data.subreddits.map(item => { return {id: item, text: item} });
                    this.setState({
                        name: res.data.name,
                        subreddits: res.data.subreddits,
                        query: res.data.query,
                        querySQL: res.data.querySQL
                    })
                }
            }.bind(this),
            catch: function(errorMsg) {
                this.setState({ errorMsg: errorMsg })
            }.bind(this)
        })

    }

    save = (e) => {

        if (e.target.getAttribute("data-enabled") !== "true") {
            return;
        }

        this.setState({ errorMsg: null })

        this.disableButton();

        let method = "POST";
        let url = "/api/channel";

        if (this.state.id !== "new") {
            method = "PUT"
            url = "/api/channel/" + this.state.id
        }

        Server.api({
            method: method,
            url: url,
            data: {
                name: this.state.name,
                subreddits: this.state.subreddits,
                query: this.state.query,
                querySQL: this.state.querySQL,
            },
            then: function(res) {
                console.log("SAVED!", res);
                this.toggle(res.data);
            }.bind(this),
            catch: function(errorMsg) {
                console.log("ERRRORED!", errorMsg);
                this.setState({ errorMsg: errorMsg })
                this.enableButton();
            }.bind(this)
        })

    }

    render() {

        return (
            <Modal isOpen={this.state.modal} toggle={this.toggle} className="channel-editor-modal">
                <ModalHeader toggle={this.toggle}>{ (this.state.id === "new") ? "Create Channel" : "Edit Channel" }</ModalHeader>
                <div className="body">
                    <div className="form-group">
                        <label for="email">Name</label>
                        <input data-enabled={this.state.buttonEnabled} onKeyPress={this.handleKeyPress} onChange={this.onInputChange} defaultValue={this.state.name} type="text" name="email" id="name" placeholder="Name of your channel" />
                    </div>
                    <div className="form-group subreddits">
                        <label for="email">Subreddits</label>
                        <span>Separate by commas or enters</span>
                        <ReactTags
                            className="tag-editor"
                            inline
                            placeholder={"add new subreddit and hit enter"}
                            minQueryLength={1}
                            tags={this.state.subreddits}
                            autofocus={false}
                            handleDelete={(j) => {
                                let subreddits = this.state.subreddits
                                subreddits = subreddits.filter((tag, index) => index !== j)
                                this.setState({ subreddits: subreddits })
                            }}
                            handleAddition={(tag) => {
                                let subreddits = this.state.subreddits
                                subreddits.push(tag)
                                this.setState({ subreddits: subreddits })
                            }}
                            delimiters={[188, 13]}
                        />
                    </div>
                    <div className="form-group">
                        <label for="email">Match Filter</label>
                        <span style={{ marginBottom: 2 }}>Add multiple rules and groups so you will only see what you really want. All posts in the above subreddit will be scanned to match these rules. All fields are case-insensitive.</span>
                        <div className="rules">
                            <QueryBuilder fields={[{name: "post", label: "post"}]} operators={[
                                { name: 'contains', label: 'contains' },
                                { name: '!contains', label: 'does not contain' },
                            ]} onQueryChange={this.logQuery} query={ this.state.query } />
                        </div>
                        { (this.state.querySQL && this.state.querySQL.length > 4) &&
                            <div className="explain">
                                {this.state.querySQL}
                            </div>
                        }
                        { (this.state.errorMsg) &&
                            <div className="error-msg">{this.state.errorMsg}</div>
                        }
                        <button data-enabled={this.state.buttonEnabled} className="button primary" onClick={(e) => { this.save(e); }}>Save</button>
                    </div>
                </div>
            </Modal>
        );

    }

}

export default ChannelEditorModal;
