const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const Schema = mongoose.Schema;
const keys = require('../config/keys');

const UsersSchema = new Schema({
    email: {
        type: String,
        unique: true
    }, // Email of the user
    hash: String, // Hash from password and salt
    salt: String, // Unique salt
}, {
	collection: 'users',
	timestamps: true
});

/**
* Generates a unique salt and hash from a plaintext password and assigns them to a user object
* @param {string} password Password in plaintext from the user
*/
UsersSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

/**
* Validates a user's password based on the salt and hash in the database
* @param {string} password Password in plaintext from the user
*/
UsersSchema.methods.validatePassword = function(password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};

/**
* Generates a JWT consisting of expiry, id and email of the user
* @return {string} JWT
*/
UsersSchema.methods.generateJWT = function() {

    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);

    return jwt.sign({
        email: this.email,
        id: this._id,
        exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, keys.jwtSecret);

}

/**
* Returns the given user's object with a JWT token included
** @return {object} details of a user
*/
UsersSchema.methods.toAuthJSON = function() {

    return {
        _id: this._id,
        email: this.email,
        token: this.generateJWT(),
    };

};

module.exports = mongoose.model('User', UsersSchema);
