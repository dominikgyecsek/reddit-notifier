const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const validator = require('validator');
var crypto = require('crypto');
const User = require('../models/User.js');
const keys = require('../config/keys');

/**
* Creates a new user
* @param {string} email email of the user (body)
* @param {string} password password of the user (body)
* @return {Object} object containing the token to be used for authorization
*/
router.post('/', auth.optional, (req, res, next) => {

    return new Promise(function(resolve, reject) {

        let user = req.body;

        console.log("SIgnup", user);

        if(!user.email || typeof user.email !== "string") {
            return reject({ code: 400, msg: "Email is required" });
        }

        if (!user.password || typeof user.password !== "string") {
            return reject({ code: 400, msg: "Password is required" });
        }

        if (!user.password.match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)) {
            return reject({ code: 400, msg: "The password has to be at least 8 characters long, must contain one number, one special character, one lower-case and one upper-case letter" });
        }

        if (!validator.isEmail(user.email)) {
            return reject({ code: 400, msg: "Invalid email address" });
        }

        const finalUser = new User({
            email: user.email
        });

        finalUser.setPassword(user.password);

        finalUser.save(function(err, user) {

            if (err) {
                return reject({ code: 400, msg: "Email is already taken" });
            } else {
                console.log("SIGNED UP");
                return resolve(finalUser.toAuthJSON())
            }

        })

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

/**
* Authenticates an existing user
* @param {string} email email of the user (body)
* @param {string} password password of the user (body)
* @return {Object} object containing the token to be used for authorization
*/
router.post('/login', auth.optional, (req, res, next) => {

    return new Promise(async function(resolve, reject) {

        console.log("SIgnup", req.body);

        req.body.type = 3

        if(!req.body.email || typeof req.body.email !== "string") {
            return reject({ code: 400, msg: "Email is required" });
        }

        if (!req.body.password || typeof req.body.password !== "string") {
            return reject({ code: 400, msg: "Password is required" });
        }

        let user = await User.findOne({email: req.body.email}).catch(e => { console.error("failed to get user", e) });

        if (!user) {
            return reject({ code: 404, msg: "Account is not found with the given details" });
        }

        if (user.validatePassword(req.body.password)) {
            return resolve(user.toAuthJSON())
        } else {
            return reject({ code: 404, msg: "Account is not found with the given details" });
        }

    }).then(function(data) {
        res.json(data)
    }).catch(function(error) {
        if ( (error) && (!error.code) ) console.error("Unexpected Error", error);
        res.status((error && error.code) ? error.code : 500).json({ error: (error && error.msg) ? error.msg : "Unexpected Error" })
    })

});

/**
* * Returns the current user's object with a JWT token included
* @param {string} id id of the user (payload)
* @return {Object} object containing the token to be used for authorization
*/
router.get('/current', auth.required, (req, res, next) => {

    const { payload: { id } } = req;

    return User.findById(id)
    .then((user) => {

        if(!user) {
            return res.sendStatus(400);
        }

        return res.json({ user: user.toAuthJSON() });

    });

});

module.exports = router;
